# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 5079, title: "Hire a contractor") do

  visit "http://zenefits.com/"
  step id: 12988,
      action: "Fill out the entire form for a new Zenefits account with dummy information (all fields need to be filled out), with email as {{ random.email }}, company zip as either 98004, 94115, or 10003, and the phone number with 10 digits; then hit submit.", 
      response: "Are you signed in?" do
    # *** START EDITING HERE ***
    click_link("Sign Up")
    fill_in("id_email", :with => "example#{Time.now.to_i}@example.net")
    fill_in("id_password", :with => "Pa$$w0rd")
    fill_in("id_companyName", :with => "Company")
    fill_in("id_companyZip", :with => ["98004","94115","10003"].sample)
    fill_in("id_companyPhone", :with => "0123456789")
    fill_in("id_regEmployeeCount", :with => "10")
    find(:css, "#id_tos").set(true)
    find("form#redirect_form input.button").click
    expect(page).to have_selector("header a", :text => "Log Out")     
    # *** STOP EDITING HERE ***
  end
  #visit "http://zenefits.com/"  this was put in by someone by mistake I think
  step id: 13084,
      action: "Click on 'hire' in the Contractors box and then click 'get started'. Go through the flow (upload anything you'd like) and fill out random information for all fields. Finally hit 'save'.", 
      response: "Are you taken back to the dashboard?" do
    # *** START EDITING HERE ***
    print has_selector?("h3", :text => "Reports") #opt
    print has_selector?(".contractor.benefit-block.clear.pale") #opt
    within(".contractor.benefit-block.clear.pale") do
      click_link("HIRE")
    end 
    click_link("Get Started")
    click_link("Hire Contractor")
    print  has_selector?("h1", :text => "Hire Contractor") #opt
    # we could probably do fill_in("ember4072".. but I think ember4072 changes often
    #so i resort to index based locating
    all("input")[0].set("John Doe")
    all("input")[1].set("example#{Time.now.to_i}@example.net")
    all("option")[1].click  #not elegent but this is the only way I could make it work
    print has_selector?("div.onboarding-page a.button", :text => "Send") #opt
    find("div.onboarding-page a.button", :text => "Send").click
    print has_selector?("h1", :text => "You're all set!") #opt
    find("a.button", :text => "Return to Dashboard").click
    #when the following two items are visible, we assume the dashboard is loaded
    expect(page).to have_selector("h3", :text => "Reports")
    expect(page).to have_selector(".hire.benefit-block.clear.pale")
    # *** STOP EDITING HERE ***
  end
end
