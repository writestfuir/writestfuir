# To execute this code, you require the rainforest_ruby_runtAime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 5085, title: "Add New Employee") do

  visit "http://zenefits.com/"
  step id: 12988,
      action: "Fill out the entire form for a new Zenefits account with dummy information (all fields need to be filled out), with email as {{ random.email }}, company zip as either 98004, 94115, or 10003, and the phone number with 10 digits; then hit submit.", 
      response: "Are you signed in?" do
    # *** START EDITING HERE ***
    click_link("Sign Up")
    fill_in("id_email", :with => "example#{Time.now.to_i}@example.net")
    fill_in("id_password", :with => "Pa$$w0rd")
    fill_in("id_companyName", :with => "Company")
    fill_in("id_companyZip", :with => ["98004","94115","10003"].sample)
    fill_in("id_companyPhone", :with => "0123456789")
    fill_in("id_regEmployeeCount", :with => "10")
    find(:css, "#id_tos").set(true)
    find("form#redirect_form input.button").click
    #when logout link is there, we then assume we are logged in
    expect(page).to have_selector("header a", :text => "Log Out")
    # *** STOP EDITING HERE ***
  end
  step id: 13090,
      action: "Click 'hire' in the Employees box and then 'get started'. Click 'add employee' on the top left, then click 'start'. Now add all random information and submit.", 
      response: "Did you see a modal indicating success? Continue to dashboard." do
    # *** START EDITING HERE ***
    within(".hire.benefit-block.clear.pale") do
      click_link("HIRE")
    end    
    click_link("Get Started")
    click_link("Add Employee")
    find("div.onboarding-page div.button", :text => "Start").click    
    puts has_selector?("h3", :text => "Report To") #optinal   
    #we locate form fields in unorthodox fashion because the
    #form fields have no easily identifiyable id/class attributes
    all("input")[0].set("John1")
    all("input")[1].set("Doe1")
    all("input")[2].set("random@random.net")
    all("input")[3].set("Random")
    all("option")[1].click  #not elegent but this is the only way I could make it work
    find("div.onboarding-page div.button", :text => "Save and Continue").click
    puts has_selector?("h3", :text => 'Salary (annual)') #optional
    fill_in(all('input')[0]['id'],:with=>"01/01/2099")
    fill_in(all('input')[1]['id'],:with=>"123000")
    find("div.button", :text => "Save & Continue").click
    expect(page).to have_content("We've sent over everything to")
    click_link("Return to Dashboard")
    # *** STOP EDITING HERE ***
  end
  step id: 13091,
      action: "On the dashboard, click 'view' in the Employees box.", 
      response: "Do you see the employee you just added with the status 'onboarding in progress'?" do
    # *** START EDITING HERE ***    
    puts has_selector?("h3", :text => "Reports") #optional
    puts has_selector?(".hire.benefit-block.clear.pale") #optional
    within(".hire.benefit-block.clear.pale") do
      click_link("VIEW")
    end
    puts has_selector?("div.primary", :text => "Doe1, John1*") #optional
    expect(page.text).to match(/Doe1, John1\* View\/Edit Details . Delete Onboarding In Progress/)
    # *** STOP EDITING HERE ***
  end
end
