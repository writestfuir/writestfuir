# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 4445, title: "CSV Export") do

  visit "https://dolla.mattermark.com/"
  step id: 11701,
      action: "Login with email: \"sarda@mattermark.com\" and password: \"qwerty\"", 
      response: "Are you logged in?" do
    # *** START EDITING HERE ***
    click_link("Log In")
    fill_in("email", :with => "sarda@mattermark.com")
    fill_in("password", :with => "qwerty")
    click_button("Sign In")
    puts page.has_css?("div#content")
    #page.save_screenshot("screenshot.png")
    expect(page.html).to match(/href=\"\/app\/account\/logout/) 
    # *** STOP EDITING HERE ***
  end
  visit "https://dolla.mattermark.com/app/data/"
  step id: 11702,
      action: "Click Location. In the modal, type \"Chicago\" and select Chicago Metropolitan Area. Click Apply Filter.", 
      response: "Do you see a green tag that says \"Region = Chicago\"? Does the entry box disappear after Apply Filter?" do
    # *** START EDITING HERE ***
    click_button("Location")
    selector = "ul.ui-autocomplete li.ui-menu-item a:contains('Metrop')"
    page.execute_script %Q{ $("#{selector}").trigger("focus"); }


    fill_in("locationCustomFilter",:with=>"Chicago")
    page.evaluate_script 'jQuery.active == 0' #wait for stuff

    #http://stackoverflow.com/questions/13187753
    #/rails3-jquery-autocomplete-how-to-test-with-rspec-and-capybara
    page.execute_script %Q{ $("#{selector}").trigger("mouseenter").trigger("click"); }
    page.execute_script %Q{ $("#{selector}").trigger("mouseenter").trigger("click"); }
 
    puts page.has_css?("div#locationModal")
    click_button("Apply")
    page.save_screenshot("screenshot.png")
    page.evaluate_script 'jQuery.active == 0' #wait for stuff
    puts page.has_no_css?("div#locationModal")
    expect( page.has_css?("div#locationModal")  ).to be false
    a = find("div.label-success",:text=>"Region = Chicago")
    expect(a.text).to match(/Region = Chicago x/)
    # *** STOP EDITING HERE ***
  end
end
