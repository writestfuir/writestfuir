# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 4438, title: "Reset Password [generic]") do

  visit "https://dolla.mattermark.com/app/Account/recover"
  step id: 11672,
      action: "Enter \"mattermarkrainforest@mailinator.com\" and click \"Send\"", 
      response: "Does the text \"You have been sent a password recovery link via email. Check your email for details on how to login and reset your password.\" appear?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.
    require 'pry'; binding.pry;


    # *** STOP EDITING HERE ***
  end
  step id: 11676,
      action: "Go to http://mailinator.com/inbox.jsp?to=mattermarkrainforest. Click on the most recent email from \"Mattermark Support\".", 
      response: "Do you see the email? Does the email have a link \"here\" in it?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
  step id: 11679,
      action: "Click on the link \"here\" in the email.", 
      response: "Does the link take you to \"Reset Your Password\"? Are there two fields to enter a new password?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
  step id: 11682,
      action: "Enter {{ random.password }} in both password fields, then click \"Change Password\".", 
      response: "Does it work?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
end
