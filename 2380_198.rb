# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#
define_variable_scope :logins do
  define_variable :client_no do
    # By default, this returns a sample value we random picked from a list of
    # possible value. If this is preventing your test from running, you can
    # change this method to generate a more appropriate value.
    "170"
  end
  define_variable :password do
    # By default, this returns a sample value we random picked from a list of
    # possible value. If this is preventing your test from running, you can
    # change this method to generate a more appropriate value.
    "83d9465c9"
  end
end

test(id: 2380, title: "Check tags save") do
  # You can use any of the following variables in your code:
  # - [&quot;logins&quot;, [&quot;client_no&quot;, &quot;password&quot;]]

  visit "https://app.rnfrstqa.com/login"
  window = Capybara.current_session.driver.browser.manage.window
  window.maximize
  step id: 10584,
      action: "Login with the email \&quot;t-{{ logins.client_no }}@e.rainforestqa.com\&quot; and the password \&quot;{{ logins.password }}\&quot;. If you are already logged in, log out first. ", 
      response: "Did you get logged in successfully? " do
    # *** START EDITING HERE ***
    if(all('.user-actions').length != 0)#if logged in
        find('.user-actions').hover
        find('.header-logout-link').click#log out
    end
    fill_in 'email', with: "t-#{logins.client_no}@e.rainforestqa.com"
    fill_in 'password', with: logins.password
    find('.login-button-child').click#login
    within(find('.header')) do
        expect(find('.user-actions'))
        expect(find('.logo'))
    end
    # *** STOP EDITING HERE ***
  end
  visit "https://app.rnfrstqa.com/runs/current"
  step id: 2633,
      action: "Click the &#39;New test&#39; button.", 
      response: "Do you see a form?" do
    # *** START EDITING HERE ***
    within(find('.create')) do
        first('a').click
    end
    within(find('.create-test-view')) do
        expect(find('form'))
    end
    # *** STOP EDITING HERE ***
  end
  step id: 2098	,
      action: "Enter a title you will be able to remember. Click \&quot;Create Test\&quot;.", 
      response: "Are you redirected to the \&quot;steps\&quot; tab?" do
    # *** START EDITING HERE ***
    title = "Winnie the Pooh Test"
    within(find('.create-test-view')) do
        fill_in 'title', with: title
        find('.create-test-button-child').click
    end
    expect(find('.steps-action-view'))
    @url = page.current_url
    # *** STOP EDITING HERE ***
  end
  visit "https://app.rnfrstqa.com/"
  step id: 5254,
      action: "Go to the test you have just created. Then, click the settings tab, then enter a new tag", 
      response: "Does a &#39;saving&#39; then a &#39;saved&#39; notification show up? (you may have to wait ~10 s)" do
    # *** START EDITING HERE ***
    visit @url
    @tag = "cool test sample tag item text"
    within(find('.test-view')) do
        find('.settings-tab').first('a').click
        div = find('.select2-choices')
        div.hover
        div.click
        within(div) do
            input = first('input')
            input.native.send_keys(@tag)
            input.native.send_keys(:enter)
        end
        #expect(find('span', :text => "Saving"))#TODO happens to quick to catch
        expect(find('span', :text => "Saved!"))
    end
    # *** STOP EDITING HERE ***
  end
  step id: 5255,
      action: "Refresh the page", 
      response: "Is the tag still there?" do
    # *** START EDITING HERE ***
    visit page.current_url
    expect(page).to have_content @tag
    # *** STOP EDITING HERE ***
  end
  step id: 5256,
      action: "Delete the tag by clicking the &#39;x&#39; on it", 
      response: "Does a &#39;saving&#39; and then a &#39;saved&#39; notification show up? (you may have to wait ~10 s)" do
    # *** START EDITING HERE ***
    within(find('.test-view')) do
        within(find('#s2id_tags')) do
            within(find('.select2-search-choice', :text => @tag)) do
                first('a').click
            end
        end
        #expect(find('span', :text => "Saving"))#TODO happens to quick to catch
        expect(find('span', :text => "Saved!"))
    end
    # *** STOP EDITING HERE ***
  end
  step id: 5257,
      action: "Refresh the page", 
      response: "Is the tag you deleted still not there?" do
    # *** START EDITING HERE ***
    visit page.current_url
    expect(page).not_to have_content @tag
    # *** STOP EDITING HERE ***
  end
end
