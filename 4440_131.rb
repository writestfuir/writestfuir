# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 4440, title: "PDF Request Emailed") do

  visit "https://dolla.mattermark.com/"
  step id: 11701,
      action: "Login with email: \"sarda@mattermark.com\" and password: \"qwerty\"", 
      response: "Are you logged in?" do
    # *** START EDITING HERE ***
    click_link("Log In")
    fill_in("email",:with => "sarda@mattermark.com")
    fill_in("password",:with => "qwerty")
    click_button("Sign In")
    puts has_content?("Featured Searches")
    # *** STOP EDITING HERE ***
  end
  step id: 11668,
      action: "In the top-right area, search for \"buzzfeed.com\" (please type this, do not copy and paste). Click the first result to load the BuzzFeed company detail page.", 
      response: "Does the company detail page load without error?  Do graphs display within a 10 seconds?" do
    # *** START EDITING HERE ***
    cnt = nil
    fill_in("nav-company-search-desktop2",:with=>"buzzfeed.com")
    a = Time.now
    find('li', :text => 'BuzzFeed buzzfeed.com').click

    #comment 1: i know sleep is not allowed
    #but this is an exceptional situation (a timing related test)
    #comment 2:it seems that capybara returns before all graphs are displayed
    #so we cant use the default capybara waiting mechanism
    #the d3 graphics library uses g.nvd3 tags wrapped in svg tags
    #wrapped in div.chart tags for displaying graphs.
    #at this point in time, they show 84 of these tags.
    #so we use that count as our test.
    50.times {
      cnt = all("div.chart div.content g.nvd3").length
      puts cnt
      sleep 1
      break if cnt >= 84
    }
    elapsed = Time.now.to_i - a.to_i
    puts cnt >= 84
    puts elapsed<=10
    # *** STOP EDITING HERE ***
  end
  step id: 11680,
      action: "Click the PDF icon on the right corner. Type in your own email in the new modal popped up. Scroll to the bottom and hit \"send\".", 
      response: "Did the email titled \"Mattermark - BuzzFeed\" with an attachment of PDF file named \"BuzzFeed.pdf\" arrive within 3 minutes?" do
    # *** START EDITING HERE ***
    require "pry"; binding.pry; 
    click_link("launchPdfPreview")
    find("div.modal-body input").set("rich_hickey@mailinator.com")
    a = Time.now
    click_link("email-pdf")
    #THE EMAIL NEVER ARRIVES
    #I WAITED 10+ MINUTES
    

    # *** STOP EDITING HERE ***
  end
end
