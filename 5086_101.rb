# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 5086, title: "Custom list rename") do

  rand_email = "sam#{rand(1000)}@example#{rand(1000)}.net"
  rand_password = rand(100000000000).to_s
  rand_first_name = "John#{rand(1000000)}"
  rand_last_name = "Doe#{rand(10000000)}"
  company_1 = nil #tagged company 1
  company_2 = nil #tagged company 2
  company_3 = nil #tagged company 3


  visit "https://dolla.mattermark.com/"
  step id: 10825,
      action: "Look at the page you are at.", 
      response: "Does the homepage load?  Is there a start free trial link in the top-right?" do
    # *** START EDITING HERE ***
    expect(page).to have_selector(".pull-right .trialheader",:text => "START FREE TRIAL")
    # *** STOP EDITING HERE ***
  end
  step id: 10826,
      action: "Click the start free trial link in the top-right", 
      response: "Does the signup page load without error?" do
    # *** START EDITING HERE ***
    click_link("Start Free Trial")
    expect(page).to have_button("Activate Free Trial")
    # *** STOP EDITING HERE ***
  end
  step id: 10827,
      action: "Sign up a new account with email as \"{{ random.email }}\" and password as \"{{ random.password }}\"", 
      response: "Does the form show your email and password filled in without any errors?" do
    # *** START EDITING HERE ***
    fill_in("signup-email", :with => rand_email)
    fill_in("signup-password", :with=> rand_password)
    fill_in("confirm-password", :with=> rand_password)   
    find("html").native.send_keys(:tab)   #trigger validations
    expect(page).to_not have_selector("label.error")
    # *** STOP EDITING HERE ***
  end
  step id: 10837,
      action: "Fill out the name, job title, company and phone number fields with fake data", 
      response: "Can the information be filled out without any problems?  " do
    # *** START EDITING HERE ***
    fill_in("first_name", :with => rand_first_name)
    fill_in("last_name", :with => rand_last_name)
    fill_in("job_title", :with => "Manager")
    fill_in("company", :with => "ABC Company")
    fill_in("phone_number", :with => "1234567890")
    find("html").native.send_keys(:tab)   #trigger validations
    expect(page).to_not have_selector("label.error")
    # *** STOP EDITING HERE ***
  end
  step id: 10838,
      action: "Fill out the credit card information.  Use 4242424242424242 for the credit card number.  Use any 03 / 18 for the expiration date.  Use any 3 digits for the security code.", 
      response: "Does the credit card info get filled out without showing any errors?" do
    # *** START EDITING HERE ***
    fill_in("number", :with => "4242424242424242")
    fill_in("exp_month", :with => "03")
    fill_in("exp_year", :with => "18")
    fill_in("cvc", :with => "123")
    find("html").native.send_keys(:tab)   #trigger validations
    expect(page).to_not have_selector("label.error")
    # *** STOP EDITING HERE **
  end
  step id: 10839,
      action: "Click Activate Free Trial", 
      response: "After clicking do you get logged into the site without error?" do
    # *** START EDITING HERE ***
    click_button("Activate Free Trial")
    has_selector?("div#sidebar-left") #ensure full page load
    find("a#copyright") #ensure full page load
    expect(page.html).to match(/href=\"\/app\/account\/logout/) 
    # *** STOP EDITING HERE ***
  end
  visit "https://dolla.mattermark.com/app/data"
  step id: 11690,
      action: "Hover over the top most company name. Hit add to custom list button and type in \"{{ random.address_city }}\" in the text box and hit Add.", 
      response: "Does a tag saying {{ random.address_city }} appear in the bottom of the hover area?" do
    # *** START EDITING HERE ***
    first("div.company_area") #ensure full page load
    find("a.last.paginate_button") #ensure full page load
    find("a#copyright") #ensure full page load
    
    company_1 =  first("div.company_area").text
    page.execute_script('$("div.company_area").first().trigger("mouseenter");')
    
    #puts page.evaluate_script('jQuery.active') #ensure ajax readiness

    #click_button("Add to Custom List")
    #find("button.dropdown-toggle").click
    #find("div.custom-list-picker button.dropdown-toggle").click
    #above three lines could not be used, fallback to js
    #find("div.custom-list-picker button.dropdown-toggle").click
    #page.execute_script('$("div.custom-list-picker button.dropdown-toggle").click()');
    find("div.custom-list-picker button.dropdown-toggle")  #ensure dialog load
    page.execute_script('$("div.custom-list-picker button.dropdown-toggle").click()');
    ##sleep 1.5
    ##page.save_screenshot("s#{Time.now.to_i}.png")
    find("input.newCustomList").set("Tokyo")    
    #find("button.addToNewCustomList").click
    page.execute_script('$("button.addToNewCustomList").click()')
    #page.save_screenshot("s.png");
    #require "pry"; binding.pry;
    #expect(page).to have_selector("a.custom-list-link", :text => "x")
    expect(page).to have_selector('a.custom-list-link', :text => "Tokyo")
    # *** STOP EDITING HERE ***
  end
  step id: 11692,
      action: "Add the next two companies to {{ random.address_city }} by hovering over the company names and hitting 'add to custom list'. Then click on the Custom List tab in the lower left.", 
      response: "Do you see the {{ random.address_city }} custom list?" do
    # *** START EDITING HERE ***

    require "pry"; binding.pry;
    page.execute_script('$("div.company_area").first().trigger("mouseleave");')
    company_2 = page.evaluate_script('$("div.company_area").eq(2).text().trim();')
    page.execute_script('$("div.company_area").eq(2).trigger("mouseenter");')
    find("div.custom-list-picker button.dropdown-toggle")  #ensure dialog load
    page.execute_script('$("div.custom-list-picker button.dropdown-toggle").click()');
    find('ul.dropdown-menu li a', :text => "Tokyo").click

    
    page.execute_script('$("div.company_area").eq(2).trigger("mouseleave");')
    company_3 = page.evaluate_script('$("div.company_area").eq(3).text().trim();')
    page.execute_script('$("div.company_area").eq(3).trigger("mouseenter");')
    

    find("div.custom-list-picker button.dropdown-toggle")  #ensure dialog load

    #bug
    page.execute_script('$("div.custom-list-picker button.dropdown-toggle").click()')
    page.save_screenshot("s#{Time.now.to_i}.png")
    #bug

    #Capybara.ignore_hidden_elements = false #responsive workaround
    #find('a', :text => "Custom Lists").click
    #Capybara.ignore_hidden_elements = true
    #expect(page).to have_selector("custom-list-page-list-elem a.list-link", :text => "Tokyo")
    # *** STOP EDITING HERE ***
  end
  step id: 11694,
      action: "Click on the {{ random.address_city }} custom list.", 
      response: "Do you see the companies that you tagged?" do
    # *** START EDITING HERE ***
    
    # *** STOP EDITING HERE ***
  end
  step id: 11699,
      action: "Hover over the companies and click on the X next to the {{ random.address_city }} tag. Do this for all three companies and do a browser refresh.", 
      response: "Is the list empty?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
  visit "https://dolla.mattermark.com/app"
  step id: 13092,
      action: "Click the Custom Lists button on the left hand side.", 
      response: "Do you see the custom list that you created?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
  step id: 13093,
      action: "Hover over the custom list. You should see a pencil. Click it and rename the test to Foo.", 
      response: "Does the list name change? When you click on the list, does it take you to a list of companies that you put in Foo?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
end
