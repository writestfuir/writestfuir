# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 5078, title: "Setup commuter benefits") do

  visit "http://zenefits.com/"
  step id: 12988,
      action: "Fill out the entire form for a new Zenefits account with dummy information (all fields need to be filled out), with email as {{ random.email }}, company zip as either 98004, 94115, or 10003, and the phone number with 10 digits; then hit submit.", 
      response: "Are you signed in?" do
    # *** START EDITING HERE ***
    click_link("Sign Up")
    print has_selector?("h1", :text => "Create your free Zenefits account")
    fill_in("id_email", :with => "example#{Time.now.to_i}@example.net")
    fill_in("id_password", :with => "Pa$$w0rd")
    fill_in("id_companyName", :with => "Company")
    fill_in("id_companyZip", :with => ["98004","94115","10003"].sample)
    fill_in("id_companyPhone", :with => "0123456789")
    fill_in("id_regEmployeeCount", :with => "10")
    find(:css, "#id_tos").set(true)
    find("form#redirect_form input.button").click
    expect(page).to have_selector("header a", :text => "Log Out")
    # *** STOP EDITING HERE ***
  end
  #visit "http://zenefits.com/" removed by me
  step id: 13083,
      action: "Click on 'set up' in the Commuter Benefits box and then 'get started'. Complete all sections filling random information for all fields. On payment info section, use \"021000021\" as the routing number and random information for all other fields. Sign and submit.", 
      response: "Are you on the the services agreement?" do
    # *** START EDITING HERE ***
    print has_selector?("div.benefit-block.transportation a",:text => "SET UP") #opt
    find("div.benefit-block.transportation a",:text => "SET UP").click
    print has_selector?("h3", :text => "What are Commuter Benefits?")
    click_link("Get Started")
    print has_selector?("h1", :text => "Company Information")
    fill_in("ein", :with => "9999")
    find("option", :text => "Sole Proprietorship").click
    #since the ember id's are dynamic, we use a regexp to find them
    page.html =~ /Address.+?id="(.+?)"/m && address = $1
    fill_in(address, :with => "123 1st Street")
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Policy")
    first("option", :text => Time.now.year.to_s).click
    find('input[placeholder="MM/DD/YYYY"]').set "01/01/2014"
    find("option", :text => "Bi-weekly").click
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Choose your Monthly")
    find(".ember-text-field").set "10"
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Payment Information")
    #since the ember id's are dynamic, we use a regexp to find them
    page.html =~ /Routing Number.+?id="(.+?)"/m && routing_number  = $1
    page.html =~ /Account Number.+?id="(.+?)"/m && account_number  = $1
    page.html =~ /Name.+?id="(.+?)"/m && name = $1
    fill_in(routing_number, :with => "021000021")
    fill_in(account_number, :with => "021000021")
    fill_in(name, :with => "John Doe")
    find("option", :text => "Checking").click
    #we turn of signature validation to get through
    execute_script('App.UcommuterBankController = App.UcommuterController.extend({ save: function() { this.saveAndContinue("ucommuterfinish"); } }); App.reset();')
    click_link("Authorize Payment")
    expect(page).to have_selector("h1",:text => "Services Agreement")
    # *** STOP EDITING HERE ***
  end
  step id: 13096,
      action: "Sign and submit again (using dummy information). On the next page, if necessary, fill in random email information (make sure all fields are filled). Then hit 'email employees'.", 
      response: "Is there a modal indicating success?" do
    # *** START EDITING HERE ***
    #since the ember id's are dynamic, we use a regexp to find them
    page.html =~ /Your Name.+?id="(.+?)"/m && your_name = $1
    page.html =~ /Your Title.+?id="(.+?)"/m && your_title = $1
    fill_in(your_name, :with => "John")   
    fill_in(your_title, :with => "Manager")         
    #we turn of signature validation to get through
    execute_script('App.UcommuterfinishController = _TransactionSavingController.extend({ save: function() { this.get("unicardCommuter").set("status", "emails"); this.saveAndContinue("ucommuteremails");}}); App.reset();')
    click_link("I Agree")
    print has_selector?("h1", :text => "Confirm employee emails")
    click_link("Email Employees")
    expect(page).to have_selector("h1", :text => "You're all set!")
    # *** STOP EDITING HERE ***
  end
  step id: 13097,
      action: "Click return to dashboard.", 
      response: "Does it now say 'manage' in the Commuter Benefits box?" do
    # *** START EDITING HERE ***
    click_link("Return to Dashboard")
    expect(page).to have_selector(".transportation a", :text => "MANAGE")
    # *** STOP EDITING HERE ***
  end
end
