# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

profile = Selenium::WebDriver::Firefox::Profile.new
profile['browser.download.folderList'] = 2 #custom location
profile['browser.download.dir'] = "/tmp"
profile['browser.helperApps.neverAsk.saveToDisk'] = "application/pdf,application/x-pdf"
profile["pdfjs.disabled"] = true
driver = Selenium::WebDriver.for :firefox, :profile => profile

test(id: 5314, title: "Upload Document to Existing Employee") do

   
  visit "http://zenefits.com/"
  step id: 12988,
      action: "Fill out the entire form for a new Zenefits account with dummy information (all fields need to be filled out), with email as {{ random.email }}, company zip as either 98004, 94115, or 10003, and the phone number with 10 digits; then hit submit.", 
      response: "Are you signed in?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
  step id: 13794,
      action: "Click 'hire' in the Employees box and then 'get started'. Click 'add employee' on the top left, then click 'start'. Now add all random information and submit.", 
      response: "Did you see a modal indicating success? Continue to dashboard." do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
  step id: 13795,
      action: "On the dashboard, click 'view' in the Employees box.", 
      response: "Do you see the employee you just added with the status 'onboarding in progress'?" do
    # *** START EDITING HERE ***
    visit 'http://www.google.com/'



    # *** STOP EDITING HERE ***
  end
  step id: 13797,
      action: "In a new tab, open \"http://www.princexml.com/samples/flyer/flyer.pdf\", download it, and close the tab.", 
      response: "Did the pdf download?" do
    # *** START EDITING HERE ***    
    require "pry"; binding.pry;    
    page.execute_script('window.open("about:blank","_blank")')
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)    
    visit "http://www.princexml.com/samples/flyer/flyer.pdf"
    page.save_screenshot("s.png")
    sleep 5
    page.save_screenshot("s2.png")
    require "pry"; binding.pry;
    # *** STOP EDITING HERE ***
  end
  step id: 13796,
      action: "Click on the View button under employees. Pick any employee, and click on the View/Edit link. Scroll down to the Documents section, and hit the \"+ Upload\" button. Label the file whatever you want, then upload the PDF you just downloaded, and hit Save.", 
      response: "Does it allow you to save these changes without any errors? Do you see the document you uploaded?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
  step id: 13798,
      action: "Click 'download' next to the document you just uploaded.", 
      response: "Does the file download?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
end
