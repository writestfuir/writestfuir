# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 5126, title: "Entrepreneur Plan Limitations (Custom List)") do

  rand_email = "sam#{rand(1000)}@example#{rand(1000)}.net"
  rand_password = rand(100000000000).to_s
  rand_first_name = "John#{rand(1000000)}"
  rand_last_name = "Doe#{Time.now.to_i}"
  list_2 = "John2#{Time.now.to_i}"
  list_3 = "John3#{Time.now.to_i}"
  list_4 = "John4#{Time.now.to_i}"
  list_5 = "John5#{Time.now.to_i}"


  visit "https://dolla.mattermark.com/app/signup?plan=entrepreneur"
  step id: 13463,
      action: "Create a new account at https://dolla.mattermark.com/app/signup?plan=entrepreneur For email address use {{ random.email }} for password use {{ random.password }} for first name use {{ random.first_name }} for last name use {{ random.last_name }} -- use the same last name value for the job title and company fields. For the phone field use 123-456-7890. For the credit card number use 4242424242424242, For expiration date month use 01, for expiration date year use 25, for Security Code use 123. Click activate free trial button.",
      response: "Are you redirected to a page titled Compare Companies Side-by-Side?" do
    # *** START EDITING HERE ***
    fill_in("signup-email", :with => rand_email)
    fill_in("signup-password", :with=> rand_password)
    fill_in("confirm-password", :with=> rand_password)
    fill_in("first_name", :with => rand_first_name)
    fill_in("last_name", :with => rand_last_name)
    fill_in("job_title", :with => rand_last_name)
    fill_in("company", :with => rand_last_name)
    fill_in("phone_number", :with => "123-456-7890")
    fill_in("number", :with => "4242424242424242")
    fill_in("exp_month", :with => "01")
    fill_in("exp_year", :with => "25")
    fill_in("cvc", :with => "123")
    click_button("Activate Free Trial")
    expect(page).to have_selector("h2", :text => "Compare Companies Side-by-Side")
    # *** STOP EDITING HERE ***
  end
  step id: 13290,
      action: "Click on Custom Lists in the lower left hand portion of the screen",
      response: "Are you directed to a page titled Custom Lists?" do
    # *** START EDITING HERE ***
    Capybara.ignore_hidden_elements = false
    find('a', :text => "Custom Lists").click
    Capybara.ignore_hidden_elements = true
    expect(page).to have_selector("h1", :text => "Custom Lists")
    # *** STOP EDITING HERE ***
  end

  step id: 13465,
      action: "Click Create New List ",
      response: "Is a modal window popped up titled New Custom List?" do
    # *** START EDITING HERE ***
    click_button("+ Create New List")
    expect(page).to have_selector(".modal h3.modal-title", :text => "New Custom List")
    # *** STOP EDITING HERE ***
  end
  step id: 13466,
      action: "In the Custom List Name field enter {{ random.first_name }}, in the List of company URLs enter mattermark.com and click Create List",
      response: "Is a green bar displayed informing you that your list was created successfully?" do
    # *** START EDITING HERE ***
    fill_in("type_code", :with => rand_first_name)
    #find("textarea.domain-list").set "mattermark.com" #works 50% time
    execute_script('$("textarea.domain-list").val("mattermark.com")') #works!
    find("button#createNewCustomList", :text => "Create List").click
    expect(page).to have_selector("div.modal-body div.alert-success", :text => "Sucess!")
    # *** STOP EDITING HERE ***
  end
  step id: 13467,
      action: "Click the Done button",
      response: "Are you returned to the Custom List page?" do
    # *** START EDITING HERE ***
    find("button.btn-primary.done", :text => "Done").click
    has_content?("Done")
    expect(page).to have_selector("h1", :text => "Custom Lists")
    expect(all("div#newModal").length).to eq(0)
    # *** STOP EDITING HERE ***
  end
  step id: 13468,
      action: "Click the Custom List link on the left side of the browser window",
      response: "Does the list you just created appear in the list of Custom Lists?" do
    # *** START EDITING HERE ***
    Capybara.ignore_hidden_elements = false
    find('a', :text => "Custom Lists").click
    Capybara.ignore_hidden_elements = true
    expect(page).to have_selector("a.list-link", :text => rand_first_name)
    # *** STOP EDITING HERE ***
  end

  puts "-"

  step id: 13465,
      action: "Click Create New List ",
      response: "Is a modal window popped up titled New Custom List?" do
    # *** START EDITING HERE ***
    click_button("+ Create New List")
    expect(page).to have_selector(".modal h3.modal-title", :text => "New Custom List")
    # *** STOP EDITING HERE ***
  end
  step id: 13466,
      action: "In the Custom List Name field enter {{ random.first_name }}, in the List of company URLs enter mattermark.com and click Create List",
      response: "Is a green bar displayed informing you that your list was created successfully?" do
    # *** START EDITING HERE ***
    fill_in("type_code", :with => list_2)
    #find("textarea.domain-list").set "mattermark.com" #works 50% time
    execute_script('$("textarea.domain-list").val("mattermark.com")') #works!
    find("button#createNewCustomList", :text => "Create List").click
    expect(page).to have_selector("div.modal-body div.alert-success", :text => "Sucess!")
    # *** STOP EDITING HERE ***
  end
  step id: 13467,
      action: "Click the Done button",
      response: "Are you returned to the Custom List page?" do
    # *** START EDITING HERE ***
    find("button.btn-primary.done", :text => "Done").click
    has_content?("Done") #useful
    expect(page).to have_selector("h1", :text => "Custom Lists")
    expect(all("div#newModal").length).to eq(0)
    # *** STOP EDITING HERE ***
  end
  step id: 13468,
      action: "Click the Custom List link on the left side of the browser window",
      response: "Does the list you just created appear in the list of Custom Lists?" do
    # *** START EDITING HERE ***
    Capybara.ignore_hidden_elements = false
    find('a', :text => "Custom Lists").click
    Capybara.ignore_hidden_elements = true
    expect(page).to have_selector("a.list-link", :text => list_2)
    # *** STOP EDITING HERE ***
  end

  puts "-"

  step id: 13465,
      action: "Click Create New List ",
      response: "Is a modal window popped up titled New Custom List?" do
    # *** START EDITING HERE ***
    click_button("+ Create New List")
    expect(page).to have_selector(".modal h3.modal-title", :text => "New Custom List")
    # *** STOP EDITING HERE ***
  end
  step id: 13466,
      action: "In the Custom List Name field enter {{ random.first_name }}, in the List of company URLs enter mattermark.com and click Create List",
      response: "Is a green bar displayed informing you that your list was created successfully?" do
    # *** START EDITING HERE ***
    fill_in("type_code", :with => list_3)
    #find("textarea.domain-list").set "mattermark.com" #works 50% time
    execute_script('$("textarea.domain-list").val("mattermark.com")') #works!
    find("button#createNewCustomList", :text => "Create List").click
    expect(page).to have_selector("div.modal-body div.alert-success", :text => "Sucess!")
    # *** STOP EDITING HERE ***
  end
  step id: 13467,
      action: "Click the Done button",
      response: "Are you returned to the Custom List page?" do
    # *** START EDITING HERE ***
    find("button.btn-primary.done", :text => "Done").click
    has_content?("Done")
    expect(page).to have_selector("h1", :text => "Custom Lists")
    expect(all("div#newModal").length).to eq(0)
    # *** STOP EDITING HERE ***
  end
  step id: 13468,
      action: "Click the Custom List link on the left side of the browser window",
      response: "Does the list you just created appear in the list of Custom Lists?" do
    # *** START EDITING HERE ***
    Capybara.ignore_hidden_elements = false
    find('a', :text => "Custom Lists").click
    Capybara.ignore_hidden_elements = true
    expect(page).to have_selector("a.list-link", :text => list_3)
    # *** STOP EDITING HERE ***
  end

  puts "-"

  step id: 13465,
      action: "Click Create New List ",
      response: "Is a modal window popped up titled New Custom List?" do
    # *** START EDITING HERE ***
    click_button("+ Create New List")
    expect(page).to have_selector(".modal h3.modal-title", :text => "New Custom List")
    # *** STOP EDITING HERE ***
  end
  step id: 13466,
      action: "In the Custom List Name field enter {{ random.first_name }}, in the List of company URLs enter mattermark.com and click Create List",
      response: "Is a green bar displayed informing you that your list was created successfully?" do
    # *** START EDITING HERE ***
    fill_in("type_code", :with => list_4)
    #find("textarea.domain-list").set "mattermark.com" #works 50% time
    execute_script('$("textarea.domain-list").val("mattermark.com")') #works!
    find("button#createNewCustomList", :text => "Create List").click
    expect(page).to have_selector("div.modal-body div.alert-success", :text => "Sucess!")
    # *** STOP EDITING HERE ***
  end
  step id: 13467,
      action: "Click the Done button",
      response: "Are you returned to the Custom List page?" do
    # *** START EDITING HERE ***
    find("button.btn-primary.done", :text => "Done").click
    has_content?("Done")
    expect(page).to have_selector("h1", :text => "Custom Lists")
    expect(all("div#newModal").length).to eq(0)
    # *** STOP EDITING HERE ***
  end
  step id: 13468,
      action: "Click the Custom List link on the left side of the browser window",
      response: "Does the list you just created appear in the list of Custom Lists?" do
    # *** START EDITING HERE ***
    Capybara.ignore_hidden_elements = false
    find('a', :text => "Custom Lists").click
    Capybara.ignore_hidden_elements = true
    expect(page).to have_selector("a.list-link", :text => list_4)
    # *** STOP EDITING HERE ***
  end

  puts "-"

  step id: 13465,
      action: "Click Create New List ",
      response: "Is a modal window popped up titled New Custom List?" do
    # *** START EDITING HERE ***
    click_button("+ Create New List")
    expect(page).to have_selector(".modal h3.modal-title", :text => "New Custom List")
    # *** STOP EDITING HERE ***
  end
  step id: 13466,
      action: "In the Custom List Name field enter {{ random.first_name }}, in the List of company URLs enter mattermark.com and click Create List",
      response: "Is a green bar displayed informing you that your list was created successfully?" do
    # *** START EDITING HERE ***
    fill_in("type_code", :with => list_5)
    #find("textarea.domain-list").set "mattermark.com" #works 50% time
    execute_script('$("textarea.domain-list").val("mattermark.com")') #works!
    find("button#createNewCustomList", :text => "Create List").click
    expect(page).to have_selector("div.modal-body div.alert-success", :text => "Sucess!")
    # *** STOP EDITING HERE ***
  end
  step id: 13467,
      action: "Click the Done button",
      response: "Are you returned to the Custom List page?" do
    # *** START EDITING HERE ***
    find("button.btn-primary.done", :text => "Done").click
    has_content?("Done")
    expect(page).to have_selector("h1", :text => "Custom Lists")
    expect(all("div#newModal").length).to eq(0)
    # *** STOP EDITING HERE ***
  end
  step id: 13468,
      action: "Click the Custom List link on the left side of the browser window",
      response: "Does the list you just created appear in the list of Custom Lists?" do
    # *** START EDITING HERE ***
    Capybara.ignore_hidden_elements = false
    find('a', :text => "Custom Lists").click
    Capybara.ignore_hidden_elements = true
    expect(page).to have_selector("a.list-link", :text => list_5)
    # *** STOP EDITING HERE ***
  end
  step id: 13292,
      action: "Click Create New List",
      response: "Does a modal pop up informing the user that his account is limited to five custom lists and provide a button to upgrade his account?" do
    # *** START EDITING HERE ***
    click_button("+ Create New List")
    expect(page).to have_content("Your current plan has a limit of five custom lists")
    expect(page).to have_selector("button", :text => "Upgrade Now")
    # *** STOP EDITING HERE ***
  end
end

