# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 5082, title: "Setup PTO Tracking") do

  visit "http://zenefits.com/"
  step id: 12988,
      action: "Fill out the entire form for a new Zenefits account with dummy information (all fields need to be filled out), with email as {{ random.email }}, company zip as either 98004, 94115, or 10003, and the phone number with 10 digits; then hit submit.", 
      response: "Are you signed in?" do
    # *** START EDITING HERE ***
    click_link("Sign Up")
    print has_selector?("h1", :text => "Create your free Zenefits account")
    fill_in("id_email", :with => "example#{Time.now.to_i}@example.net")
    fill_in("id_password", :with => "Pa$$w0rd")
    fill_in("id_companyName", :with => "Company")
    fill_in("id_companyZip", :with => ["98004","94115","10003"].sample)
    fill_in("id_companyPhone", :with => "0123456789")
    fill_in("id_regEmployeeCount", :with => "10")
    find(:css, "#id_tos").set(true)
    find("form#redirect_form input.button").click
    expect(page).to have_selector("header a", :text => "Log Out")
    # *** STOP EDITING HERE ***
  end
  step id: 13089,
      action: "Click on 'setup' in the PTO Tracking box. Click on 'manually enter employee info' at the top. Add two employees with any random information for those fields. Submit. Go back to the dashboard (click the Zenefits logo on the top left).", 
      response: "Click 'view' in the Employees box. Are the two employees added?" do
    # *** START EDITING HERE ***
    print has_selector?("div.benefit-block.pto a",:text => "SET UP") #opt
    find("div.benefit-block.pto a",:text => "SET UP").click
    print has_selector?("h1", :text => "PTO Setup")
    click_link("manually enter your employee info")    
    print has_selector?("input.flat", :minimum => 8)
    all("input.flat")[0].set "John1"
    all("input.flat")[1].set "Doe1"
    all("input.flat")[2].set "35"
    all("input.flat")[3].set "90402"
    all("input.flat")[4].set "Jane1"
    all("input.flat")[5].set "Doe1"
    all("input.flat")[6].set "35"
    all("input.flat")[7].set "90402"
    click_link("I'm Done")
    print has_selector?("h1", :text => "Setup Your PTO Policy")
    find("a.logo", :text => "Zenefits").click
    print has_selector?("div.benefit-block.hire a",:text => "VIEW") #opt
    find("div.benefit-block.hire a",:text => "VIEW").click    
    print has_selector?("table.employeelist", :minimum => 2)
    expect(page.text).to match(/Doe1, John1 View\/Edit Details/)
    expect(page.text).to match(/Doe1, Jane1 View\/Edit Details/)
    # *** STOP EDITING HERE ***
  end
  step id: 13098,
      action: "Go back to the dashboard (click the Zenefits logo). Click on 'resume' in the PTO Tracking box. Select 'yes' for limiting the number of days employees can take off. Go through the entire flow, filling in random information for all fields and select 'yes' when possible. On the last section, click 'finish setup'.", 
      response: "Do you see dashboard where you can email employees?" do
    # *** START EDITING HERE ***
    find("a.logo", :text => "Zenefits").click
    #NOTE THERES A MISTAKE IN THE STEP DEFINITION - THE BUTTON CAPTION IS ACTUALLY "SET UP"
    print has_selector?("div.benefit-block.pto a",:text => "SET UP") #opt
    find("div.benefit-block.pto a",:text => "SET UP").click
    print has_selector?("h1", :text => "Setup Your PTO Policy")
    all("input.ember-radio-button")[0].click    
    all("input.ember-radio-button")[2].click
    all("input.ember-text-field")[0].set "10"
    click_link("Save & Continue")
    print has_selector?("h1", :text => "Sick & Personal Leave")
    all("input.ember-radio-button")[1].click
    all("input.ember-radio-button")[3].click
    click_link("Save & Update")
    print has_selector?("h1", :text => "Import PTO Data")
    click_link("Skip")
    print has_selector?("h1", :text => "Manage Categories of Time Off")
    click_link("Continue")
    print has_selector?("h1", :text => "Setup Your PTO Holiday")
    click_link("Finish Setup")
    expect(page).to have_selector("strong", :text => "Confirm employee emails")
    expect(page).to have_selector("input.ember-text-field", :minimum => 2)
    # *** STOP EDITING HERE ***
  end
  step id: 13099,
      action: "Add dummy, valid email addresses where necessary. Click 'email employees'.", 
      response: "Do you see a modal indicating success?" do
    # *** START EDITING HERE ***
    all("input.flat")[2].set "test1@example.net"
    all("input.flat")[5].set "test2@example.net"
    click_link("Email Employees")
    expect(page).to have_selector("h1", :text => "You're all set!")
    # *** STOP EDITING HERE ***
  end
end
