# -*- coding: utf-8 -*-
# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 5310, title: "Change Contributions mid-BoR") do

  visit "http://zenefits.com/"
  step id: 12988,
      action: "Fill out the entire form for a new Zenefits account with dummy information (all fields need to be filled out), with email as {{ random.email }}, company zip as either 98004, 94115, or 10003, and the phone number with 10 digits; then hit submit.", 
      response: "Are you signed in?" do
    # *** START EDITING HERE ***
    click_link("Sign Up")
    fill_in("id_email", :with => "example#{Time.now.to_i}@example.net")
    fill_in("id_password", :with => "Pa$$w0rd")
    fill_in("id_companyName", :with => "Company")
    fill_in("id_companyZip", :with => ["98004","94115","10003"].sample)
    fill_in("id_companyPhone", :with => "0123456789")
    fill_in("id_regEmployeeCount", :with => "10")
    find(:css, "#id_tos").set(true)
    find("form#redirect_form input.button").click
    expect(page).to have_selector("header a", :text => "Log Out")    
    # *** STOP EDITING HERE ***
  end
  step id: 12990,
      action: "Click on ‘Get quotes’ button in the ‘Medical | Dental | Vision’ box on the top right. Opt to manually enter your employee info. Fill in random information for two employees and submit.", 
      response: "Are you presented with a view to select medical plans?" do
    # *** START EDITING HERE ***
    print has_selector?("div.benefit-block.health a",:text => "GET QUOTES") #opt
    find(".health a", :text => "GET QUOTES").click
    print has_selector?("h1", :text => "Get health insurance quotes. Instantly.")
    click_link("manually enter your employee info")
    print has_selector?("strong", :text => "Tell us about your employees")
    print has_selector?("input.flat", :minimum => 8)
    all("input.flat")[0].set "John"
    all("input.flat")[1].set "Doe"
    all("input.flat")[2].set "35"
    all("input.flat")[3].set "90402"
    all("input.flat")[4].set "Jane"
    all("input.flat")[5].set "Doe"
    all("input.flat")[6].set "35"
    all("input.flat")[7].set "90402"
    click_link("I'm Done")
    print has_selector?("h1", :text => "First, select the medical plans you'd like to off")
    #it takes quite a few seconds for the rows to show up
    print has_selector?("section .middle .controls a.remove", :minimum => 1)
    # *** STOP EDITING HERE ***
  end
  step id: 12991,
      action: "Remove all of the current medical plans so your current package has zero plans. Now try to continue to dental and vision plans.", 
      response: "Is the button greyed out and when you clicked it, does a warning message appear indicating that you must have at least one medical plan to continue?" do
    # *** START EDITING HERE ***
    cnt =  all("a.remove").count
    cnt.times { |a|
      all("a.remove").last.click
      print has_no_selector?("a.remove", :count => (cnt-a))
    }
    print has_selector?("a.button.grey", :text => "Offer These Medical Plans")

    find("a.button.grey", :text => "Offer These Medical Plans").click
    print has_selector?("p.input-error", :text => "select at least one medical plan")
    # *** STOP EDITING HERE ***
  end
  step id: 12992,
      action: "Add a plan below and continue to dental and vision plans. Feel free to add any plan and continue until you get to a page that has a \"Begin Enrollment\" button on it. Click on it.", 
      response: "Are you directed to a page that has Health Insurance Enrollment as the heading with a \"Begin Enrollment\" button?" do
    # *** START EDITING HERE ***
    print has_selector?(".controls a.add", :minimum => 1)
    all(".controls a.add")[0].click
    sleep 10
    print has_selector?("h3", :text => "Plan added to package")
    find("a.button", :text => "Finish and Enroll").click
    print has_selector?("h1", :text => "Select a company dental plan to offer")
    click_link("Don't Offer Dental")
    print has_selector?("h1", :text => "Select a company vision plan to offer")
    click_link("Don't Offer Vision")
    print has_selector?("h1", :text => "Company Health Insurance Enrollment")
    print has_selector?("a.button", :text =>"Begin Enrollment")
    # *** STOP EDITING HERE ***
  end
  step id: 13553,
      action: "Click on \"Begin Enrollment\". Go through the Enrollment process, picking any  Coverage start date, and yes for the question regarding 6 weeks of payroll. For the \"How much of the premium will the company pay?\" page, fill in whatever you like and hit continue. Do the same for Dental and Vision insurance.", 
      response: "Did you get directed to a page that asks for your admin contact?" do
    # *** START EDITING HERE ***
    click_link("Begin Enrollment")
    print has_selector?("h1", :text => "Company Policy")
    all("option")[1].click
    #choose("Yes") didnt work
    #no choice but to go against guieline"Don't assume elements ordering"
    all("input.ember-radio-button")[0].click
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Company Policy")
    all("option", :text => "Percentage")[0].click  
    all("input.ember-text-field")[0].set "50"
    all("input.ember-text-field")[1].set "50"
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Admin Contact")
    # *** STOP EDITING HERE ***
  end
  step id: 13554,
      action: "Fill in admin contact. Complete the company info section. This is 6 steps. For EIN, use 12-345678. For SIC code, use 9999. For routing number, use 313180918. Fill out and sign the next page. ", 
      response: "Does this take you to a page where you can create email addresses for your employees?" do
    # *** START EDITING HERE ***
    print "\n"; page.save_screenshot(".png"); require "pry"; binding.pry;    
    all("input")[0].set "John Doe"
    all("input")[1].set "Manager"
    all("input")[2].set "example@example.net"
    all("input")[3].set "1234567890"
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Company InfoStep 1 of 8")
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Company InfoStep 2 of 8")
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Company InfoStep 3 of 8")
    click_link("Save and Continue")
    all("input.ember-text-field")[0].set "12-345678"
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Company InfoStep 4 of 8")
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Company InfoStep 5 of 8")
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Company InfoStep 6 of 8")
    all("input.ember-text-field")[0].set "9999"
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Company InfoStep 7 of 8")
    click_link("Confirm and Continue")
    print has_selector?("h1", :text => "Company InfoStep 8 of 8")
    all("input.ember-text-field")[0].set "Abc Bank"
    all("input.ember-text-field")[1].set "313180918"
    all("input.ember-text-field")[2].set "12345678"
    click_link("Agree and Continue")



    # *** STOP EDITING HERE ***
  end
  step id: 13555,
      action: "Create email addresses. Edit one of the employees email address as \"{{ random.first_name }}{{ random.number }}@e.rainforestqa.com\". Click \"Email Employees Now\" on the bottom of the next page. Continue to fill out next pages however you'd like. Click on the \"Return to Dashboard\" button.", 
      response: "Do you return to the dashboard with the Medical Insurance card in the top right NOT saying \"setup\"?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
  step id: 13784,
      action: "Click on Manage under the Medical insurance enrollment card. Click on change contributions. Change values of each field (dropdown values, and percentage, or $values), and hit Save.", 
      response: "Does this take you back to the overview page?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
end

