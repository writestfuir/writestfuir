# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 4966, title: "Add New Hire") do

  visit "http://zenefits.com/"
  step id: 12988,
      action: "Fill out the entire form for a new Zenefits account with dummy information (all fields need to be filled out), with email as {{ random.email }}, company zip as either 98004, 94115, or 10003, and the phone number with 10 digits; then hit submit.", 
      response: "Are you signed in?" do
    # *** START EDITING HERE ***
    click_link("Sign Up")
    print has_selector?("h1", :text => "Create your free Zenefits account")
    fill_in("id_email", :with => "example#{Time.now.to_i}@example.net")
    fill_in("id_password", :with => "Pa$$w0rd")
    fill_in("id_companyName", :with => "Company")
    fill_in("id_companyZip", :with => "10003")
    fill_in("id_companyPhone", :with => "0123456789")
    fill_in("id_regEmployeeCount", :with => "10")
    find(:css, "#id_tos").set(true)
    find("form#redirect_form input.button").click
    expect(page).to have_selector("header a", :text => "Log Out")
    # *** STOP EDITING HERE ***
  end
  step id: 12989,
      action: "Click on hire in the employees box, then get started. Click on add employee and continue through this flow by adding dummy information and submitting the form.", 
      response: "Did a modal indicate that you are successful in adding that new employee?" do
    # *** START EDITING HERE ***
    print has_selector?(".hire.benefit-block.clear.pale a", :text => "HIRE")
    within(".hire.benefit-block.clear.pale") do
      click_link("HIRE")
    end    
    click_link("Get Started")
    click_link("Add Employee")
    find("div.onboarding-page div.button", :text => "Start").click
    print has_selector?("h2", :text => "Contact Information")
    print has_selector?("input", :minimum => 4)
    #since the ember id's are dynamic, we use a regexp to find them
    page.html =~ /First Name.+?id="(.+?)"/m && first_name = $1
    page.html =~ /Last Name.+?id="(.+?)"/m && last_name = $1
    page.html =~ /Email.+?id="(.+?)"/m && email = $1
    page.html =~ /Title.+?id="(.+?)"/m && title = $1
    fill_in(first_name, :with => "John")
    fill_in(last_name, :with => "Doe")
    fill_in(email, :with => "example@example.net")
    fill_in(title, :with => "Supervisor")
    find("option", :text => "New York").click
    find("div.button", :text => "Save and Continue").click
    print has_selector?("h1", :text => "Employment Information")
    #since the ember id's are dynamic, we use a regexp to find them
    page.html =~ /Date<.+?id="(.+?)"/m && start_date = $1
    page.html =~ /Salary.+?id="(.+?)"/m && salary = $1
    fill_in(start_date, :with => "01/01/2005")
    fill_in(salary, :with => "100000")
    find("div.button", :text => "Save & Continue").click
    expect(page).to have_selector("h1", :text => "We've sent over everything")
    # *** STOP EDITING HERE ***
  end
end
