# -*- coding: iso-8859-1 -*-
# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 4964, title: "Add existing health insurance") do

  visit "http://zenefits.com/"
  step id: 12988,
      action: "Fill out the entire form for a new Zenefits account with dummy information (all fields need to be filled out), with email as {{ random.email }}, company zip as either 98004, 94115, or 10003, and the phone number with 10 digits; then hit submit.", 
      response: "Are you signed in?" do
    # *** START EDITING HERE ***
    click_link("Sign Up")
    print has_selector?("h1", :text => "Create your free Zenefits account")
    fill_in("id_email", :with => "example#{Time.now.to_i}@example.net")
    fill_in("id_password", :with => "Pa$$w0rd")
    fill_in("id_companyName", :with => "Company")
    fill_in("id_companyZip", :with => "10003")
    fill_in("id_companyPhone", :with => "0123456789")
    fill_in("id_regEmployeeCount", :with => "10")
    find(:css, "#id_tos").set(true)
    find("form#redirect_form input.button").click
    expect(page).to have_selector("header a", :text => "Log Out")
    # *** STOP EDITING HERE ***
  end
  step id: 12986,
      action: "Click the 'add existing' button in the 'medical / dental / vision' box on the upper right and go through the flow. Feel free to randomly select all of the fields and continue hitting next. At the last page, fill the form, add your signature, and submit. ", 
      response: "Do you receive a modal indicating success? Click continue." do
    # *** START EDITING HERE ***
    print has_selector?("div.benefit-block.liability a",:text => "ADD EXISTING") #opt
    find(".liability a", :text => "ADD EXISTING").click
    print has_selector?("h1", :text => "Add your current health insurance")
    all("option", :text => "Aetna")[0].click
    all("option", :text => "Aetna")[1].click
    print has_selector?("input.policy", :count => "2")
    all("input.policy")[0].set "123456"
    all("input.policy")[1].set "123456"
    find("a.button", :text => "Next").click #click_link("Next") didnt work
    print has_selector?("h1", :text => "Lastly, assign Zenefits as your primary")
    sleep 5
    all("input")[0].set "John Doe"
    sleep 0.5
    all("input")[1].set "Manager"
    sleep 0.5
    all("input")[2].set "www.example.net"
    sleep 0.5
    all("input")[3].set "(434) 343-4334"
    sleep 0.5
    print "\n"; save_screenshot(".png"); require "pry"; binding.pry;           
    #we turn off signature validation, because we cant actually draw
    page.execute_script('$.getScript("https://s3-us-west-1.amazonaws.com/pub1133/jquery.signaturepad.js")')
    #above its the jquery Signature-Pad plugin,
    #but with lines 714, 731,732 and 734 commented out.


    # *** STOP EDITING HERE ***
  end
  step id: 12987,
      action: "Fill in the form (providing any contribution type and amount) and continue through the flow until you find a link on the bottom right that allows you to finish later. Click on it.", 
      response: "Are you redirected to the main dashboard and does the 'medical / dental / vision' box reflect that the existing health insurance information is being imported?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
end
