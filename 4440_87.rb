# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 4440, title: "PDF Request Emailed") do

  visit "https://dolla.mattermark.com/"
  step id: 11701,
      action: "Login with email: \"sarda@mattermark.com\" and password: \"qwerty\"", 
      response: "Are you logged in?" do
    # *** START EDITING HERE ***
    click_link("Log In")
    fill_in("email",:with => "sarda@mattermark.com")
    fill_in("password",:with => "qwerty")
    click_button("Sign In")
    expect(page).to have_content("Featured Searches")
    # *** STOP EDITING HERE ***
  end
  step id: 11668,
      action: "In the top-right area, search for \"buzzfeed.com\" (please type this, do not copy and paste). Click the first result to load the BuzzFeed company detail page.", 
      response: "Does the company detail page load without error?  Do graphs display within a 10 seconds?" do
    # *** START EDITING HERE ***
    fill_in("nav-company-search-desktop2",:with=>"buzzfeed.com")
    a = Time.now
    find('li', :text => 'BuzzFeed buzzfeed.com').click
    b = Time.now
    elapsed = b.to_i - a.to_i
    puts "Took #{elapsed}s"
    expect(elapsed).to be <= 20
    # *** STOP EDITING HERE ***
  end
  step id: 11680,
      action: "Click the PDF icon on the right corner. Type in your own email in the new modal popped up. Scroll to the bottom and hit \"send\".", 
      response: "Did the email titled \"Mattermark - BuzzFeed\" with an attachment of PDF file named \"BuzzFeed.pdf\" arrive within 3 minutes?" do
    # *** START EDITING HERE ***
    require 'pry'; binding.pry
    click_link("launchPdfPreview")
    find("div.modal-body input").set("rich_hickey@mailinator.com")
    click_link("email-pdf")
    a = Time.now
    




    # *** STOP EDITING HERE ***
  end
end
