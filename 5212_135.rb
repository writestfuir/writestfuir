# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 5212, title: "Entrepreneur Plan Signup") do

  visit "https://dolla.mattermark.com/app/signup?plan=entrepreneur"
  step id: 13463,
      action: "Create a new account at https://dolla.mattermark.com/app/signup?plan=entrepreneur For email address use {{ random.email }} for password use {{ random.password }} for first name use {{ random.first_name }} for last name use {{ random.last_name }} -- use the same last name value for the job title and company fields. For the phone field use 123-456-7890. For the credit card number use 4242424242424242, For expiration date month use 01, for expiration date year use 25, for Security Code use 123. Click activate free trial button.", 
      response: "Are you redirected to a page titled Compare Companies Side-by-Side?" do
    # *** START EDITING HERE ***
    rand_email = "sam#{rand(1000)}@example#{rand(1000)}.net"
    rand_password = rand(100000000000).to_s
    rand_first_name = "John#{rand(1000000)}"
    rand_last_name = "Doe#{Time.now.to_i}"
    fill_in("signup-email", :with => rand_email)
    fill_in("signup-password", :with=> rand_password)
    fill_in("confirm-password", :with=> rand_password)
    fill_in("first_name", :with => rand_first_name)
    fill_in("last_name", :with => rand_last_name)
    fill_in("job_title", :with => rand_last_name)
    fill_in("company", :with => rand_last_name)
    fill_in("phone_number", :with => "123-456-7890")
    fill_in("number", :with => "4242424242424242")
    fill_in("exp_month", :with => "01")
    fill_in("exp_year", :with => "25")
    fill_in("cvc", :with => "123")
    click_button("Activate Free Trial")
    expect(page).to have_selector("h2", :text => "Compare Companies Side-by-Side")
    # *** STOP EDITING HERE ***
  end
end
