# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 5313, title: "Edit Employee Information") do
  puts "start"
  f_name = "John"
  l_name = "Doe#{rand(10000000)}"
  visit "http://zenefits.com/"
  step id: 12988,
      action: "Fill out the entire form for a new Zenefits account with dummy information (all fields need to be filled out), with email as {{ random.email }}, company zip as either 98004, 94115, or 10003, and the phone number with 10 digits; then hit submit.", 
      response: "Are you signed in?" do
    # *** START EDITING HERE ***
    puts "step"
    click_link("Sign Up")
    page.evaluate_script 'jQuery.active == 0' #wait for jquery
    #http://stackoverflow.com/questions/
    #8801845/how-to-make-capybara-check-for-visibility-after-some-js-has-run
    fill_in("companyPhone", :with => "0123456789")
    fill_in("companyZip"  , :with => "90402")
    fill_in("companyName" , :with => "Abc")
    find(:css, "input#id_tos").set(true)
    fill_in("password"    , :with => "Pa$$w0rd")
    fill_in("email"       , :with => "abc#{rand(1000)}@example.net")
    fill_in("regEmployeeCount", :with => rand(1000))
    click_button("Sign Up")
    page.evaluate_script 'jQuery.active == 0'  #wait for jquery  
    expect(page).to have_link("Log Out")
    # *** STOP EDITING HERE ***
  end
  step id: 13791,
      action: "Click 'hire' in the Employees box and then 'get started'. Click 'add employee' on the top left, then click 'start'. Now add all random information and submit.", 
      response: "Did you see a modal indicating success? Continue to dashboard." do
    # *** START EDITING HERE ***
    puts "step"
    within(".hire.benefit-block.clear.pale") do
      click_link("HIRE")
    end
    page.evaluate_script 'jQuery.active == 0'   #wait for jquery  
    click_link("Get Started")
    page.evaluate_script 'jQuery.active == 0'   #wait for jquery  
    click_link("Add Employee")
    page.evaluate_script 'jQuery.active == 0'   #wait for jquery  
    find("div.button", :text => "Start").click
    page.evaluate_script 'jQuery.active == 0'   #wait for jquery  

    #it seems zenefits.com uses ember.js , and a lot of html
    #elements have no meaningful id attribute or a unique class

    fill_in "ember3944", :with => f_name
    fill_in "ember3946", :with => l_name
    fill_in "ember3947", :with => "#{rand(10000000)}@test.com"
    fill_in "ember3949", :with => "test"
    Capybara.ignore_hidden_elements = false
    #here we choose first item in select/dropdown
    all('#ember3954 option')[1].select_option
    Capybara.ignore_hidden_elements = true
    page.evaluate_script 'jQuery.active == 0'   #wait for jquery  
    find("div.button", :text => "Save and Continue").click
    page.evaluate_script 'jQuery.active == 0'  #wait for jquery  
    expect(page).to have_content("Start Date")
    #they have random/cryptic  ember ids for the text fields
    #fill_in "ember4456", :with => "01/01/2099"
    #i found that 4456 above changes on every run so
    #ended up with code below
    fill_in(all('input')[0]['id'],:with=>"01/01/2099")
    fill_in(all('input')[1]['id'],:with=>"123000")
    find("div.button", :text => "Save & Continue").click
    page.evaluate_script 'jQuery.active == 0'  #wait for jquery  
    expect(page).to have_content "We've sent over everything to"
    click_link("Return to Dashboard")
    page.evaluate_script 'jQuery.active == 0'  #wait for jquery  
    # *** STOP EDITING HERE ***
  end
  step id: 13792,
      action: "Click 'view' in the Employees box.", 
      response: "Do you see the employee you just added with the status 'onboarding in progress'?" do
    # *** START EDITING HERE ***
    puts "step"
    within(".hire.benefit-block.clear.pale") do
      click_link("VIEW")
    end
    page.evaluate_script 'jQuery.active == 0'  #wait for jquery
    #row = all("tr").select { |x| x.text.include?"#{l_name}, #{f_name}" }
    #expect(row.last.text).to match(/Onboarding In Progress/)
    expect(page.text).to match(/#{l_name}, #{f_name}\* View\/Edit Details . Delete Onboarding In Progress/)
    # *** STOP EDITING HERE ***
  end
  step id: 13793,
      action: "Click on the View button under employees. Pick any employee, and click on the View/Edit link. Click on the edit details button for either the Personal Information or Employment and Compensation sections. Edit whatever values you like, and hit save at the top right.", 
      response: "Does it allow you to save these changes without any errors?" do
    # *** START EDITING HERE ***
    puts "step"
    within(".employeelist-row") do
      first("a", :text => "View/Edit Details").click
    end
    page.evaluate_script 'jQuery.active == 0'  #wait for jquery  
    random_ssn_a = "123-12-"
    random_ssn_b = rand(1000..9999).to_s
    random_ssn = random_ssn_a + random_ssn_b
    first("div.button", :text => "EDIT DETAILS").click
    page.evaluate_script 'jQuery.active == 0'  #wait for jquery  

    #it seems zenefits.com uses ember.js , and a lot of html
    #elements have no meaningful id attribute or a unique class

    #the social security field is the tenth input field from top
    fill_in(all('input.ember-text-field')[10]['id'],:with=>random_ssn)
    first("div.button", :text => "SAVE").click
    page.evaluate_script 'jQuery.active == 0'  #wait for jquery
    #http://stackoverflow.com/questions/
    #8801845/how-to-make-capybara-check-for-visibility-after-some-js-has-run  
    expect(page).to have_content("***-**-#{random_ssn_b}")
    #note: the few few digits are masked by them
    # *** STOP EDITING HERE ***
  end
end
