# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 5213, title: "Create Custom List Subtest") do

  random_name = "Sam#{rand(1000000)}"

  visit "https://dolla.mattermark.com/app/account/lists"
  step id: 13465,
      action: "Click Create New List ", 
      response: "Is a modal window popped up titled New Custom List?" do
    # *** START EDITING HERE ***
    #the following three lines I had to add, even though not mentioned
    fill_in("email", :with => "sarda@mattermark.com")
    fill_in("password", :with => "qwerty")
    click_button("Sign In")
    click_button("+ Create New List")
    expect(page).to have_selector("h3.modal-title", :text => "New Custom List")
    # *** STOP EDITING HERE ***
  end
  step id: 13466,
      action: "In the Custom List Name field enter {{ random.first_name }}, in the List of company URLs enter mattermark.com and click Create List", 
      response: "Is a green bar displayed informing you that your list was created successfully?" do
    # *** START EDITING HERE ***
    fill_in("type_code", :with => random_name)
    find("textarea.domain-list").set "mattermark.com"
    click_button("Create List")
    expect(page).to have_selector("div.alert-success", :text => "Sucess!")
    # *** STOP EDITING HERE ***
  end
  step id: 13467,
      action: "Click the Done button", 
      response: "Are you returned to the Custom List page?" do
    # *** START EDITING HERE ***
    click_button("Done")
    expect(page).to_not have_selector("div.modal.fade.in")
    # *** STOP EDITING HERE ***
  end
  step id: 13468,
      action: "Click the Custom List link on the left side of the browser window", 
      response: "Does the list you just created appear in the list of Custom Lists?" do
    # *** START EDITING HERE ***
    click_link("Custom Lists")
    expect(page).to have_selector("a.list-link", :text => random_name)
    # *** STOP EDITING HERE ***
  end
end
