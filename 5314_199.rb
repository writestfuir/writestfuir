# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 5314, title: "Upload Document to Existing Employee") do

  visit "http://zenefits.com/"
  step id: 12988,
      action: "Fill out the entire form for a new Zenefits account with dummy information (all fields need to be filled out), with email as {{ random.email }}, company zip as either 98004, 94115, or 10003, and the phone number with 10 digits; then hit submit.", 
      response: "Are you signed in?" do
    # *** START EDITING HERE ***
    click_link("Sign Up")
    print has_selector?("h1", :text => "Create your free Zenefits account")
    fill_in("id_email", :with => "example#{Time.now.to_i}@example.net")
    fill_in("id_password", :with => "Pa$$w0rd")
    fill_in("id_companyName", :with => "Company")
    fill_in("id_companyZip", :with => "10003")
    fill_in("id_companyPhone", :with => "0123456789")
    fill_in("id_regEmployeeCount", :with => "10")
    find(:css, "#id_tos").set(true)
    find("form#redirect_form input.button").click
    expect(page).to have_selector("header a", :text => "Log Out")
    # *** STOP EDITING HERE ***
  end
  step id: 13794,
      action: "Click 'hire' in the Employees box and then 'get started'. Click 'add employee' on the top left, then click 'start'. Now add all random information and submit.", 
      response: "Did you see a modal indicating success? Continue to dashboard." do
    # *** START EDITING HERE ***
    print has_selector?(".hire.benefit-block.clear.pale a", :text => "HIRE")
    within(".hire.benefit-block.clear.pale") do
      click_link("HIRE")
    end
    print has_selector?("h1", :text => "Hire Employees In Minutes")    
    click_link("Get Started")
    print has_selector?("h1", :text => "Configure Hiring Process")    
    click_link("Add Employee")    
    print has_selector?("div.button", :text => "Start")
    find("div.onboarding-page div.button", :text => "Start").click
    print has_selector?("h2", :text => "Contact Information")
    print has_selector?("input", :minimum => 4)
    #since the ember id's are dynamic, we use a regexp to find them
    page.html =~ /First Name.+?id="(.+?)"/m && first_name = $1
    page.html =~ /Last Name.+?id="(.+?)"/m && last_name = $1
    page.html =~ /Email.+?id="(.+?)"/m && email = $1
    page.html =~ /Title.+?id="(.+?)"/m && title = $1
    fill_in(first_name, :with => "John")
    fill_in(last_name, :with => "Doe")
    fill_in(email, :with => "example@example.net")
    fill_in(title, :with => "Supervisor")
    find("option", :text => "New York").click
    find("div.button", :text => "Save and Continue").click
    print has_selector?("h1", :text => "Employment Information")
    #since the ember id's are dynamic, we use a regexp to find them
    page.html =~ /Date<.+?id="(.+?)"/m && start_date = $1
    page.html =~ /Salary.+?id="(.+?)"/m && salary = $1
    fill_in(start_date, :with => "01/01/2005")
    fill_in(salary, :with => "100000")
    find("div.button", :text => "Save & Continue").click
    expect(page).to have_selector("h1", :text => "We've sent over everything")
    click_link("Return to Dashboard")
    # *** STOP EDITING HERE ***
  end
  step id: 13795,
      action: "On the dashboard, click 'view' in the Employees box.", 
      response: "Do you see the employee you just added with the status 'onboarding in progress'?" do
    # *** START EDITING HERE ***
    within(".hire.benefit-block.clear.pale") do
      click_link("VIEW")
    end
    print has_selector?("div.primary", :text => "Doe, John*")
    expect(page.text).to match(/Doe, John\* View\/Edit Details . Delete Onboarding In Progress/)
    # *** STOP EDITING HERE ***
  end
  step id: 13797,
      action: "In a new tab, open \"http://www.princexml.com/samples/flyer/flyer.pdf\", download it, and close the tab.", 
      response: "Did the pdf download?" do
    # *** START EDITING HERE ***
    #According to stackoverflow http://stackoverflow.com/questions/8851051/
    #"...WebDriver cannot interact with dialogs - this is because dialogs are
    #the domain of the operating system and not the webpage..."
    #so here, we download the file in a way that is compatible with capybara
    File.write("/tmp/flyer.pdf", Net::HTTP.get(URI.parse("http://www.princexml.com/samples/flyer/flyer.pdf")))
    a = File.exist?("/tmp/flyer.pdf")
    expect(a).to be true
    # *** STOP EDITING HERE ***
  end
  step id: 13796,
      action: "Click on the View button under employees. Pick any employee, and click on the View/Edit link. Scroll down to the Documents section, and hit the \"+ Upload\" button. Label the file whatever you want, then upload the PDF you just downloaded, and hit Save.", 
      response: "Does it allow you to save these changes without any errors? Do you see the document you uploaded?" do
    # *** START EDITING HERE ***
    find("a", :text => "View/Edit Details").click
    find("div.button", :text => "+ UPLOAD").click
    find("div.upload-document input").set "flyer.pdf"
    find("a.upload-area").click
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    Capybara.ignore_hidden_elements = false
    attach_file("fileUploadInput", '/tmp/flyer.pdf')
    #the window closes when upload is complete
    #so we wait 10 secs (for upload) or till window closes, whichever comes first
    20.times {
      print page.driver.browser.window_handles.length
      sleep 0.5   #waiting for file upload
      break if page.driver.browser.window_handles.length==1
    }
    Capybara.ignore_hidden_elements = true
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.first)
    print has_selector?("div.button", :text => "Save")
    find("div.button", :text => "Save").click
    has_no_selector?("div.button", :text => "Save")
    expect(page).to have_content("flyer.pdf Download Delete")
    # *** STOP EDITING HERE ***
  end
  step id: 13798,
      action: "Click 'download' next to the document you just uploaded.", 
      response: "Does the file download?" do
    # *** START EDITING HERE ***
    print has_selector?("div.title", :text => "Documents")
    click_link("Download")
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    expect(page).to have_content("WARM BEAUTIFUL APARTMENT")
    # *** STOP EDITING HERE ***
  end
end
