# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 4620, title: "Sign Up via email on Sign Up Page") do

  visit "http://crowdtilt.com/"
  step id: 12166,
      action: "If you are already logged in (your user name is on the header), log out.", 
      response: "Are you logged out? Do you now see a Log In link on the header?" do
    # *** START EDITING HERE ***
    if has_css?('div.link-user')
      #this means someone logged in
      find('div.link-user').click
      click_link('Log out')
    end
    expect(page).to have_link('Log in')
    # *** STOP EDITING HERE ***
  end
  visit "http://crowdtilt.com/signup"
  step id: 12206,
      action: "Choose to sign up with an email address", 
      response: "Does a sign up form appear? Does the form include name, email, and password? Are there a Term of Use link and a Privacy Policy link?" do
    # *** START EDITING HERE ***
    expect(page).to have_css('#signup-form')
    expect(page).to have_field('fullname')
    expect(page).to have_field('email')
    expect(page).to have_field('signup-password')
    expect(page).to have_link('Terms of Use')
    expect(page).to have_link('Privacy Policy')
    # *** STOP EDITING HERE ***
  end
  step id: 12208,
      action: "Sign up using \"rainforest+livetest-{{ date_time.yyyymmdd }}-{{ random.number }}@crowdtilt.com\" (email), \"pWEXywxnY9!\" (password), and \"{{ random.full_name }}\" (name).", 
      response: "Are you redirected to the home page? Are you logged in? Do you see the user name on the header?" do
    # *** START EDITING HERE ***
    #require 'pry'; binding.pry;
    random_name =  "#{(0...8).map { (65 + rand(26)).chr }.join} #{(0...8).map { (65 + rand(26)).chr }.join }"
    fill_in('email',:with => "rainforest+livetest-#{Time.now.strftime("%Y%m%d")}-#{rand(10000)}@crowdtilt.com")
    fill_in("signup-password",:with => "pWEXywxnY9!")
    fill_in("fullname",:with => random_name)
    click_button('Sign Up')
    sleep 3
    expect(current_url).to eq("https://www.tilt.com/")
    expect(find(:css,'.user-name').text).to eq(random_name)
    # *** STOP EDITING HERE ***
  end
end
