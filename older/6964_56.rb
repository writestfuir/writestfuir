# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 702, title: "Sign up") do

  visit "https://app.rnfrstqa.com/"
  step id: 6964,
      action: "If you are logged in, log out. If you are not logged in, do nothing.", 
      response: "Do you see a login page?" do
    # *** START EDITING HERE ***
    if has_css?('section.user-actions')
      #this means user logged in
      all('a')[3].click
      click_link('Logout')
    end
    # *** STOP EDITING HERE ***
  end
  step id: 1223,
      action: "Click on \"Sign up\"", 
      response: "Do you see a form?" do
    # *** START EDITING HERE ***
    click_link('Signup')
    expect(page).to have_css('form.signup')
    # *** STOP EDITING HERE ***
  end
  step id: 1070,
      action: "Fill in the entire form using test details (email: \"{{ random.email }}\", password: \"{{ random.password }}\", company: \"Rainforest QA\", URL: \"http://rainforestqa.com\"). If you make any errors fix them. Click Sign Up.", 
      response: "Did you get logged in and redirected to the application?" do
    # *** START EDITING HERE ***
    fill_in('site_url', :with => "http://rainforestqa.com")
    fill_in('name', :with => "Rainforest QA")
    fill_in('email', :with => "#{rand}@#{rand}.com}")
    fill_in('password', :with => "#{rand(100000000)}")
    find('button.signup-button-child').click
    expect(current_url=="https://app.rnfrstqa.com/signup").to be false
    # *** STOP EDITING HERE ***
  end
end
