# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#
define_variable_scope :crowdtilt_facebook_user do
  define_variable :email do
    # By default, this returns a sample value we random picked from a list of
    # possible value. If this is preventing your test from running, you can
    # change this method to generate a more appropriate value.
    "rainforest+facebook-2@crowdtilt.com"
  end
  define_variable :password do
    # By default, this returns a sample value we random picked from a list of
    # possible value. If this is preventing your test from running, you can
    # change this method to generate a more appropriate value.
    "pWEXywxnY9!"
  end
end

test(id: 4573, title: "Create a Sell Something campaign") do
  # You can use any of the following variables in your code:
  # - ["crowdtilt_facebook_user", ["email", "password"]]

  visit "http://crowdtilt.com/"
  step id: 12166,
      action: "If you are already logged in (your user name is on the header), log out.", 
      response: "Are you logged out? Do you now see a Log In link on the header?" do
    # *** START EDITING HERE ***
    if has_css?('div.link-user')
      #this means someone logged in
      find('div.link-user').click
      click_link('Log out')
    end
    expect(page).to have_link('Log in')  
    # *** STOP EDITING HERE ***
  end
  step id: 12112,
      action: "Click Log In on the header", 
      response: "Does a Log In lightbox open? Are there a Term of Use link and a Privacy Policy link?" do
    # *** START EDITING HERE ***
    require 'pry'; binding.pry
    click_link('Log in')
    expect( find("div#global-login-popup")['style'] ).to include("visible")
    expect(page).to have_link('Terms of Use')
    expect(page).to have_link('Privacy Policy')
    # *** STOP EDITING HERE ***
  end
  step id: 12113,
      action: "Click email option", 
      response: "Does an email and password form appear? Is there a forgot password link?" do
    # *** START EDITING HERE ***
    expect(page).to have_field('email')
    expect(page).to have_field('password')
    expect(page).to have_css('span.forgot-badge')
    # *** STOP EDITING HERE ***
  end
  step id: 12114,
      action: "Log in with \"{{ crowdtilt_facebook_user.email }}\" and \"{{ crowdtilt_facebook_user.password }}\"", 
      response: "Does the lightbox close? Are you logged in? Do you see the user name on the header?" do
    # *** START EDITING HERE ***
    require 'pry'; binding.pry  
    fill_in('email', :with =>  crowdtilt_facebook_user.email)
    fill_in('password', :with => crowdtilt_facebook_user.password)    
    click_button('Log In')
    expect(find(:css,'.user-name').text).to eq("Caden Hoeger")
    page.should_not have_css('div#global-login-popup')
    # *** STOP EDITING HERE ***
  end
  visit "http://crowdtilt.com/campaigns/new"
  step id: 12162,
      action: "Select Sell Something", 
      response: "Does a campaign creation form appear?" do
    # *** START EDITING HERE ***
    


    # *** STOP EDITING HERE ***
  end
  step id: 12163,
      action: "Fill out the form the way you like it. The title should be \"Test Sell {{ random.number }}\". Choose the \"private\" option. Create the campaign when you are ready.", 
      response: "Do you see your new campaign page now? Is there a lightbox with helpful tips on your screen?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
  step id: 12164,
      action: "Close the tips lightbox", 
      response: "Does the lightbox close? Does anything on the campaign look normal (nothing strange or out of place)?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
end
