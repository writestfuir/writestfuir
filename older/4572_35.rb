require 'pry'
# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 4572, title: "Basic email server check") do

  visit "https://e.rainforestqa.com/1"
  step id: 12160,
      action: "Send an email to '{{ random.number }}@e.rainforestqa.com'. Wait ~20 seconds.", 
      response: "Go to https://e.rainforestqa.com/{{ random.number }} - do you see the message?" do
    # *** START EDITING HERE ***
    ran = rand(100000)
    require 'rest_client'
    RestClient.post "https://api:key-fed172d5ecf4d6d1ffb83a7bf1d838a3"\
    "@api.mailgun.net/v2/sandbox87c762e88ba94141b08a2f127ac47728.mailgun.org/messages",
    :from => "Mailgun Sandbox <postmaster@sandbox87c762e88ba94141b08a2f127ac47728.mailgun.org>",
    :to => "#{ran}@e.rainforestqa.com",
    :subject => "thisIsAPeiceOfText",
    :text => "thisIsAPeiceOfText"    
    #my mail server takes 20 secs to deliver a mail.
    sleep 20
    visit "https://e.rainforestqa.com/#{ran}"
    expect(page).to have_content("thisIsAPeiceOfText")

    # *** STOP EDITING HERE ***
  end
end
