# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 4436, title: "Mattermark filters, Stage, Location, Co-Investor") do

  visit "https://dolla.mattermark.com/"
  step id: 11701,
      action: "Login with email: \"sarda@mattermark.com\" and password: \"qwerty\"", 
      response: "Are you logged in?" do
    # *** START EDITING HERE ***
    click_link 'Log In'  
    fill_in :email, with: "sarda@mattermark.com"
    fill_in :password, with: "qwerty"  
    click_button "Sign In"
    expect(page).to have_content("Featured Searches")    
    # *** STOP EDITING HERE ***
  end
  visit "https://dolla.mattermark.com/app/data"
  step id: 11670,
      action: "Click on the Location button. Type in \"India\" in the text box and click on the India (Country) result. Click Apply Filter.", 
      response: "Does the number of entries decreases?" do
    # *** START EDITING HERE ***
    find('#mattermark_info').text =~ /of (.*?) entries/
    entries_before = $1.gsub(',','').to_i
    find_by_id('btnLocation').click
    fill_in('locationCustomFilter', :with => 'India')
    sleep 1
    all('a.autocomplete-type-label').select { |a|
      a.text == 'India Country'
    }[0].click
    sleep 1
    click_button('Apply')
    find('#mattermark_info').text =~ /of (.*?) entries/
    entries_after = $1.gsub(',','').to_i
    expect(entries_after).to be < entries_before
    # *** STOP EDITING HERE ***
  end
  step id: 11671,
      action: "Click on the 'Stage' _BUTTON_ at the top. Click 'No Known Funding' and 'Pre Series A' filter. Hit Apply Filter. Sort on Stage column so that series A is on top (may take a few seconds to load).", 
      response: "Are the top most companies Series A?" do
    # *** START EDITING HERE ***
    find_by_id("btnStage").click
    all("input")[1].click
    all("input")[2].click
    click_button("Apply")
    all("th").select { |a|
      a.text == "Stage"
    }[0].click    
    sleep 1
    all("th").select { |a|
      a.text == "Stage"
    }[0].click    
    sleep 1
    cell = find(:css, 'table.company-list tr.odd:nth-child(1) td:nth-child(6)')
    expect(cell.text).to include("Series A")
    # *** STOP EDITING HERE ***
  end
  step id: 11674,
      action: "Click on the 'Vertical' button at the top. Select 'banking' checkbox. Hit Apply Filter.", 
      response: "Do you see fewer companies in the list?  It's okay if there are now 0 companies." do
    # *** START EDITING HERE ***
    find('#mattermark_info').text =~ /of (.*?) entries/
    entries_before = $1.gsub(',','').to_i
    find_by_id("btnTag").click
    find(:css, "input[value='Banking']").set(true)
    click_button("Apply")
    find('#mattermark_info').text =~ /of (.*?) entries/
    entries_after = $1.gsub(',','').to_i
    expect(entries_after).to be < entries_before
    # *** STOP EDITING HERE ***
  end
  step id: 11677,
      action: "Click the Advanced button.  This is the right most orange button; it is between a button with a wrench and a + sign. Delete all the filters except the topmost one.", 
      response: "Does the list of companies become larger?  It may take up to 10 seconds for the results to update." do
    # *** START EDITING HERE ***
    find('#mattermark_info').text =~ /of (.*?) entries/
    entries_before = $1.gsub(',','').to_i
    (0..3).each do
      find_by_id("btnMoreFilter").click
      find(:css, ".subtract-query-row").click
      find_link('Apply').click
    end
    find('#mattermark_info').text =~ /of (.*?) entries/
    entries_after = $1.gsub(',','').to_i
    expect(entries_after).to be > entries_before
    # *** STOP EDITING HERE ***
  end
end
