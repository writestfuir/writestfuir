# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#
define_variable_scope :logins do
  define_variable :client_no do
    # By default, this returns a sample value we random picked from a list of
    # possible value. If this is preventing your test from running, you can
    # change this method to generate a more appropriate value.
    "170"
  end
  define_variable :password do
    # By default, this returns a sample value we random picked from a list of
    # possible value. If this is preventing your test from running, you can
    # change this method to generate a more appropriate value.
    "83d9465c9"
  end
end

test(id: 3009, title: "Menu icons") do
  # You can use any of the following variables in your code:
  # - ["logins", ["client_no", "password"]]

  visit "https://app.rnfrstqa.com/login"
  step id: 10584,
      action: "Login with the email \"t-{{ logins.client_no }}@e.rainforestqa.com\" and the password \"{{ logins.password }}\". If you are already logged in, log out first. ", 
      response: "Did you get logged in successfully? " do
    # *** START EDITING HERE ***
    if has_css?('section.user-actions')
      #this means user logged in
      all('a')[3].click
      click_link('Logout')
    end
    fill_in :email, with: "t-#{logins.client_no}@e.rainforestqa.com"
    fill_in :password, with: logins.password
    click_button 'Login'
    expect(page).to have_css('section.user-actions')    
    # *** STOP EDITING HERE ***
  end
  step id: 6965,
      action: "Click on the menu in the top right hand corner.", 
      response: "Is there an icon next to each menu item? " do
    # *** START EDITING HERE ***
    find('section.user-actions').click
    all('a.header-settings-link').each { |item|
      expect(item['innerHTML']).to include("icon")
    }
    # *** STOP EDITING HERE ***
  end
  step id: 6966,
      action: "Click on a menu icon", 
      response: "Are you redirected to the appropriate page?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.
    url_before = current_url
    #now we click on a 3rd menu item
    all('a.header-settings-link')[3].click
    expect(current_url==url_before).to be false 
    # *** STOP EDITING HERE ***
  end
end
