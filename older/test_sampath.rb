require 'rubygems'
require "sauce"
require "sauce/capybara"
require "pry"
require 'rspec'
require 'rspec/expectations'

include RSpec::Matchers

# Set up configuration
Sauce.config do |c|
  c[:browsers] = [ ["Windows 7", "Firefox", "20"] ]
end

driver = ENV.fetch("CAPYBARA_DRIVER") { "selenium" }
Capybara.default_driver = :"#{driver}"
Capybara.default_wait_time = 20

include Capybara::DSL
results = 0
found = false
puts "Step 1"
visit "https://app.rnfrstqa.com/"
if page.has_css?('.login-view')
  puts "       Not logged in so doing nothing."
else
  puts "       Logging out"
  find(:css, '.header-logout-link').click
end

puts "Step 2"
if page.has_css?('.header-logout-link')
  puts "       Already logged in, so log out"
end
fill_in('email', :with => 'test-client@rainforestqa.com')
fill_in('password', :with => 'password')
find(:css, '.login-button-child').click
page.should have_css('.create')

puts "Step 3"
visit "https://app.rnfrstqa.com/tests/"
sleep 5
puts "       #{page.all(:css, '.small-test').count} tests in dashboard"
page.all(:css, '.small-test').each do |el|
    el.click
    sleep 10
    puts "       \"#{el.text}\" has #{page.all(:css, '.step').count} steps"    
    if page.all(:css, '.step').count>0
      found=true
      break     
    end
end
found.should be true    

puts "Step 4"
page.should have_css('.cut.disabled')
  puts "       Clicking checkbox"
page.all(:css, '.bulk-edit').first.click
saved_val_1 = page.all(:css, '.step-text')[0].text
page.should have_no_css('.cut.disabled')
puts "       Actions are now un-greyed"

puts "Step 5"
page.should have_no_css('.select2-container')
puts "       Clicking Copy button"
find(:css, 'li.copy').click
sleep 2
page.should have_css('.select2-container')

puts "Step 6"
find(".select2-offscreen option:eq(2)") rescue abort("Dropdown is empty")
puts "       Copying to \"#{find(".select2-offscreen option:eq(2)").text}\""
saved_val_2 = find(".select2-offscreen option:eq(2)").text
page.should have_no_css('li.copy.success')
find(".select2-offscreen option:eq(2)").click
page.should have_css('li.copy.success')
puts "       Button turned green with a tick"

puts "Step 7"
page.all(:css, '.small-test').each do |el|
    el.click
    sleep 1
    if el.text == saved_val_2   
      puts "       \"#{el.text}\" now as #{page.all(:css, '.step').count} steps"      
      results = page.all(:css, '.step').select { |x|
        x.text[1..-1] == saved_val_1[1..-1] 
      }.count
      puts "       Previously added step was there" if results>0
      results.should be > 0
      break
    end
end

exit

