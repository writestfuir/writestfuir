# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 1114, title: "Sign up from the marketing site") do
  prev_form = "" #to share state across multiple steps

  visit "https://www.rnfrstqa.com/"
  step id: 2836,
      action: "Look at the page", 
      response: "Do you see a button that says \"Sign up for free\" or \"Try it for Free\" or something similar?" do
    # *** START EDITING HERE ***
    expect(page).to have_link("Sign Up for Free").or have_link("Try it for Free")
    # *** STOP EDITING HERE ***
  end
  step id: 2837,
      action: "Click the button you just found", 
      response: "Do you see a form?" do
    # *** START EDITING HERE ***
    btn = find_link("Try it for Free") if has_link?("Try it for Free")
    btn = find_link("Sign Up for Free") if has_link?("Sign Up for Free")
    btn.click
    expect(page).to have_css("form.signup")
    prev_form = find("form.signup")["innerHTML"] #store for later use
    # *** STOP EDITING HERE ***
  end
  step id: 2838,
      action: "Go back and look for the 'sign up' (or similar) link at the top of the page. Click it.", 
      response: "Do you see the same sign up form as before?" do
    # *** START EDITING HERE ***
    #my assumption: Go Back means the back button of browser
    #http://stackoverflow.com/questions/
    #3888131/how-can-i-simulate-the-browser-back-button-in-capybara
    page.evaluate_script('window.history.back()')
    #my assumption: Go Back means the back button of browser
    btn = find_link("Try it for Free") if has_link?("Try it for Free")
    btn = find_link("Sign Up for Free") if has_link?("Sign Up for Free")
    btn.click
    afte_form = find("form.signup")["innerHTML"]
    expect(prev_form).to eq(afte_form)
    # *** STOP EDITING HERE ***
  end
end
