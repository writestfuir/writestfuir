# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 4569, title: "Sign Up via email in a lightbox") do

  visit "http://crowdtilt.com/"
  step id: 12166,
      action: "If you are already logged in (your user name is on the header), log out.", 
      response: "Are you logged out? Do you now see a Log In link on the header?" do
    # *** START EDITING HERE ***
    if has_css?('div.link-user')
      #this means someone logged in
      find('div.link-user').click
      click_link('Log out')
    end
    expect(page).to have_link('Log in')  
    # *** STOP EDITING HERE ***
  end
  step id: 12136,
      action: "Click Log In on the header", 
      response: "Does a Log In lightbox appear?" do
    # *** START EDITING HERE ***
    click_link('Log in')
    expect( find("div#global-login-popup")['style'] ).to include("visible")
    # *** STOP EDITING HERE ***
  end
  step id: 12139,
      action: "Choose create account", 
      response: "Does the lightbox now show the sign up options? Are there a Term of Use link and a Privacy Policy link?" do
    # *** START EDITING HERE ***
    click_link('Sign up')
    expect(page).to have_css('#signup-form')
    expect(page).to have_field('fullname')
    expect(page).to have_field('email')
    expect(page).to have_field('signup-password')
    expect(page).to have_link('Terms of Use')
    expect(page).to have_link('Privacy Policy')
    # *** STOP EDITING HERE ***
  end
  step id: 12137,
      action: "Click email option", 
      response: "Does a sign up form appear? Does the form include email, password, and name?" do
    # *** START EDITING HERE ***

    #not sure what "Click email option" means above.
    expect(page).to have_field('fullname')
    expect(page).to have_field('email')
    expect(page).to have_field('signup-password')
    # *** STOP EDITING HERE ***
  end
  step id: 12138,
      action: "Sign up using \"rainforest+livetest-{{ date_time.yyyymmdd }}-{{ random.number }}@crowdtilt.com\" (email), \"pWEXywxnY9!\" (password), and \"{{ random.full_name }}\" (name).", 
      response: "Does the lightbox close? Are you logged in? Do you see the user name on the header?" do
    # *** START EDITING HERE ***
    #require 'pry'; binding.pry
    random_name =  "#{(0...8).map { (65 + rand(26)).chr }.join} #{(0...8).map { (65 + rand(26)).chr }.join }"
    fill_in('email',:with => "rainforest+livetest-#{Time.now.strftime("%Y%m%d")}-#{rand(10000)}@crowdtilt.com")
    fill_in("signup-password",:with => "pWEXywxnY9!")
    fill_in("fullname",:with => random_name)
    click_button('Sign Up')
    expect(find(:css,'.user-name').text).to eq(random_name)
    page.should_not have_css('div#global-login-popup')
    # *** STOP EDITING HERE ***
  end
end
