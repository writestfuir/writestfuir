# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#
define_variable_scope :crowdtilt_facebook_user do
  define_variable :email do
    # By default, this returns a sample value we random picked from a list of
    # possible value. If this is preventing your test from running, you can
    # change this method to generate a more appropriate value.
    "rainforest+facebook-2@crowdtilt.com"
  end
  define_variable :password do
    # By default, this returns a sample value we random picked from a list of
    # possible value. If this is preventing your test from running, you can
    # change this method to generate a more appropriate value.
    "pWEXywxnY9!"
  end
end

test(id: 4621, title: "Log In via email on Log In Page") do
  # You can use any of the following variables in your code:
  # - ["crowdtilt_facebook_user", ["email", "password"]]

  visit "http://crowdtilt.com/"
  step id: 12166,
      action: "If you are already logged in (your user name is on the header), log out.", 
      response: "Are you logged out? Do you now see a Log In link on the header?" do
    # *** START EDITING HERE ***
    if has_css?('div.link-user')
      #this means someone logged in
      find('div.link-user').click
      click_link('Log out')
    end
    expect(page).to have_link('Log in')
    # *** STOP EDITING HERE ***
  end
  visit "http://crowdtilt.com/login"
  step id: 12213,
      action: "Inspect the page", 
      response: "Do you see a log in form (email and password)? Is there a forgot-password link? Do you see a terms of use link and a privacy policy links?" do
    # *** START EDITING HERE ***
    expect(page).to have_field('email')
    expect(page).to have_field('password')
    expect(page).to have_css('span.forgot-badge')
    expect(page).to have_link('Terms of Use')
    expect(page).to have_link('Privacy Policy')
    # *** STOP EDITING HERE ***
  end
  step id: 12212,
      action: "Log in with \"{{ crowdtilt_facebook_user.email }}\" and \"{{ crowdtilt_facebook_user.password }}\"", 
      response: "Are you redirected to the home page? Are you logged in? Do you see the user name on the header?" do
    # *** START EDITING HERE ***
    fill_in('email', :with =>  crowdtilt_facebook_user.email)
    fill_in('password', :with => crowdtilt_facebook_user.password)    
    click_button('Log In')
    sleep 3
    expect(current_url).to eq("https://www.tilt.com/")
    expect(find(:css,'.user-name').text).to eq("Caden Hoeger")
    # *** STOP EDITING HERE ***
  end
end
