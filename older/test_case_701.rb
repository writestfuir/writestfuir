# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#
define_variable_scope :logins do
  define_variable :client_no do
    # By default, this returns a sample value we random picked from a list of
    # possible value. If this is preventing your test from running, you can
    # change this method to generate a more appropriate value.
    "170"
  end
end

test(id: 701, title: "Login (expect failure)") do
  # You can use any of the following variables in your code:
  # - ["logins", ["client_no"]]

  visit "https://app.rnfrstqa.com/"
  step id: 2839,
      action: "If you are logged in, log out. If you are not logged in do nothing.", 
      response: "Do you see a login page?" do
    # *** START EDITING HERE *** 
    if page.has_css?('a.title')
      find('a.title').click
      find(:css, '.header-logout-link').click
    end
    if page.has_content?("Login to Rainforest")
      #do nothing
    end    
    expect(page).to have_content('Login to Rainforest')
    # *** STOP EDITING HERE ***
  end
  visit "https://app.rnfrstqa.com/login"
  step id: 1222,
      action: "Login with \"t-{{ logins.client_no }}@e.rainforestqa.com\" and password \"{{ random.password }}\"", 
      response: "Did you get logged-in?" do
    # *** START EDITING HERE ***
    fill_in('email', :with =>  "#{logins.client_no}@e.rainforestqa.com")
    fill_in('password', :with => "#{rand(10000).to_s}")
    find(:css, '.login-button-child').click
    expect(page).to have_content('Invalid details')
    # *** STOP EDITING HERE ***
  end
end
