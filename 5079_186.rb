# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 5079, title: "Hire a contractor") do

  visit "http://zenefits.com/"
  step id: 12988,
      action: "Fill out the entire form for a new Zenefits account with dummy information (all fields need to be filled out), with email as {{ random.email }}, company zip as either 98004, 94115, or 10003, and the phone number with 10 digits; then hit submit.", 
      response: "Are you signed in?" do
    # *** START EDITING HERE ***
    click_link("Sign Up")
    print has_selector?("h1", :text => "Create your free Zenefits account")
    fill_in("id_email", :with => "example#{Time.now.to_i}@example.net")
    fill_in("id_password", :with => "Pa$$w0rd")
    fill_in("id_companyName", :with => "Company")
    fill_in("id_companyZip", :with => ["98004","94115","10003"].sample)
    fill_in("id_companyPhone", :with => "0123456789")
    fill_in("id_regEmployeeCount", :with => "10")
    find(:css, "#id_tos").set(true)
    find("form#redirect_form input.button").click
    expect(page).to have_selector("header a", :text => "Log Out")
    # *** STOP EDITING HERE ***
  end
  #visit "http://zenefits.com/"  somebody put this here by mistake i think -Sam
  step id: 13084,
      action: "Click on 'hire' in the Contractors box and then click 'get started'. Go through the flow (upload anything you'd like) and fill out random information for all fields. Finally hit 'save'.", 
      response: "Are you taken back to the dashboard?" do
    # *** START EDITING HERE ***
    print has_selector?(".contractor.benefit-block.clear.pale a", :text => "HIRE")    
    within(".contractor.benefit-block.clear.pale") do
      click_link("HIRE")
    end
    click_link("Get Started")
    click_link("Hire Contractor")

    #note1
    #so to fill in the text fields in this page, we have three options
    #1) use an index based selector
    #2) use xpath selector
    #3) use the id attribute of the input fields
    #i choose #2 on this occation because you guys sometimes reject #1 sometimes
    #and the id attribute mentioned in #3 is unreliable / changes very often

    #note2
    #the ember.js input boxes have no identifying information such as id or class attributes
    #so we resort to xpath

    within(:xpath, '//h3[contains(string(), "Name:")]/following-sibling::div[1]') { #please see note2 for reasoning for using xpath
      find("input").set "John Doe"  #please see note1 for reasoning for using xpath
    }
    within(:xpath, '//h3[contains(string(), "Email:")]/following-sibling::div[1]') { #please see note2 for reasoning for using xpath
      find("input").set "example@example.net"
    }
    find("option", :text => "No Agreement").click
    print has_selector?("a", :text => "Send")
    click_link("Send")
    print has_selector?("h1", :text => "You're all set!")
    click_link("Return to Dashboard")
    #now we check for some items that usually appear on the dashboard
    expect(page).to have_selector("h3", :text => "Reports")
    expect(page).to have_selector(".hire.benefit-block.clear.pale")
    # *** STOP EDITING HERE ***
  end
end
