# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 5313, title: "Edit Employee Information") do

  visit "http://zenefits.com/"
  step id: 12988,
      action: "Fill out the entire form for a new Zenefits account with dummy information (all fields need to be filled out), with email as {{ random.email }}, company zip as either 98004, 94115, or 10003, and the phone number with 10 digits; then hit submit.", 
      response: "Are you signed in?" do
    # *** START EDITING HERE ***
    page.evaluate_script 'jQuery.active == 0' #wait for jquery 
    rand_email = "abc#{rand(100)}@example#{rand(100)}.net"
    click_link("Sign Up")
    page.evaluate_script 'jQuery.active == 0' #wait for jquery 
    fill_in("companyPhone", :with => "0123456789")
    fill_in("companyZip"  , :with => "98004")
    fill_in("companyName" , :with => "Abc#{rand(1000)}")
    find(:css, "input#id_tos").set(true)
    fill_in("password"    , :with => "Pa$$w0rd")
    fill_in("email"       , :with => rand_email)
    fill_in("regEmployeeCount", :with => rand(1000))
    click_button("Sign Up")
    page.evaluate_script 'jQuery.active == 0' #wait for jquery 
    expect(page.text).to match(/#{rand_email}/)
    expect(page.text).to match(/Log Out/)
    # *** STOP EDITING HERE ***
  end
  step id: 13791,
      action: "Click 'hire' in the Employees box and then 'get started'. Click 'add employee' on the top left, then click 'start'. Now add all random information and submit.", 
      response: "Did you see a modal indicating success? Continue to dashboard." do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
  step id: 13792,
      action: "Click 'view' in the Employees box.", 
      response: "Do you see the employee you just added with the status 'onboarding in progress'?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
  step id: 13793,
      action: "Click on the View button under employees. Pick any employee, and click on the View/Edit link. Click on the edit details button for either the Personal Information or Employment and Compensation sections. Edit whatever values you like, and hit save at the top right.", 
      response: "Does it allow you to save these changes without any errors?" do
    # *** START EDITING HERE ***
    # Replace this comment with the code for this action and response here.



    # *** STOP EDITING HERE ***
  end
end
