# -*- coding: utf-8 -*-
# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 5307, title: "Add Dependent") do
  random_email =  "john#{rand(100000000000)}@e.rainforestqa.com"    

  visit "http://zenefits.com/"
  step id: 12988,
      action: "Fill out the entire form for a new Zenefits account with dummy information (all fields need to be filled out), with email as {{ random.email }}, company zip as either 98004, 94115, or 10003, and the phone number with 10 digits; then hit submit.", 
      response: "Are you signed in?" do
    # *** START EDITING HERE ***
    click_link("Sign Up")
    print has_selector?("h1", :text => "Create your free Zenefits account")
    fill_in("id_email", :with => "example#{Time.now.to_i}@example.net")
    fill_in("id_password", :with => "Pa$$w0rd")
    fill_in("id_companyName", :with => "Company")
    fill_in("id_companyZip", :with => "98004")
    fill_in("id_companyPhone", :with => "0123456789")
    fill_in("id_regEmployeeCount", :with => "10")
    find(:css, "#id_tos").set(true)
    find("form#redirect_form input.button").click
    expect(page).to have_selector("header a", :text => "Log Out")
    # *** STOP EDITING HERE ***
  end
  step id: 12990,
      action: "Click on ‘Get quotes’ button in the ‘Medical | Dental | Vision’ box on the top right. Opt to manually enter your employee info. Fill in random information for two employees and submit.", 
      response: "Are you presented with a view to select medical plans?" do
    # *** START EDITING HERE ***
    print has_selector?("div.health a", :text => "GET QUOTES")
    find("div.health a", :text => "GET QUOTES").click
    print has_selector?("h1", :text => "Instantly")
    click_link("manually enter your employee info")
    print has_selector?("h1", :text => "Tell us about your employees")
    all("input[placeholder='First Name']")[0].set "John"
    all("input[placeholder='First Name']")[1].set "Jane"
    all("input[placeholder='Last Name']")[0].set "Doe"
    all("input[placeholder='Last Name']")[1].set "Doe"
    all("div.age-range input")[0].set "35"
    all("div.age-range input")[1].set "35"
    all("div.zip input")[0].set "98004"
    all("div.zip input")[1].set "98004"
    click_link("I'm Done")
    expect(page).to have_selector("h1", :text => "select the medical plans")
    # *** STOP EDITING HERE ***
  end
  step id: 12991,
      action: "Remove all of the current medical plans so your current package has zero plans. Now try to continue to dental and vision plans.", 
      response: "Is the button greyed out and when you clicked it, does a warning message appear indicating that you must have at least one medical plan to continue?" do
    # *** START EDITING HERE ***
    print has_selector?("a.plan-link", :minimum => 3)
    print has_selector?("a.remove", :minimum => 3)
    cnt =  all("a.remove").count
    cnt.downto(1) { |a|
      all("a.remove").last.click
      print has_selector?("a.remove", :count => a-1)
    }
    expect(page).to have_selector("a.grey", :text => "Offer These Medical Plans")
    find("a.grey", :text => "Offer These Medical Plans").click
    expect(page).to have_selector(".input-error", :text => "Please select at least one medical plan to continue.")
    # *** STOP EDITING HERE ***
  end
  step id: 12992,
      action: "Add a plan below and continue to dental and vision plans. Feel free to add any plan and continue until you get to a page that has a \"Begin Enrollment\" button on it. Click on it.", 
      response: "Are you directed to a page that has Health Insurance Enrollment as the heading with a \"Begin Enrollment\" button?" do
    # *** START EDITING HERE ***
    first(".controls a.add").click
    print has_link?("Finish and Enroll")
    click_link("Finish and Enroll")
    print has_link?("skip vision")
    click_link("skip vision")
    expect(page).to have_link("Begin Enrollment")
    expect(page).to have_selector("h1", :text => "Health Insurance Enrollment") 
    # *** STOP EDITING HERE ***
  end
  step id: 13553,
      action: "Click on \"Begin Enrollment\". Go through the Enrollment process, picking any  Coverage start date, and yes for the question regarding 6 weeks of payroll. For the \"How much of the premium will the company pay?\" page, fill in whatever you like and hit continue. Do the same for Dental and Vision insurance.", 
      response: "Did you get directed to a page that asks for your admin contact?" do
    # *** START EDITING HERE ***
    click_link("Begin Enrollment")
    print has_selector?("h3", :text => "Coverage Start Date")
    find("input[name=havePayrollHistory][value=yes]").click
    all("option")[1].click
    click_link("Save and Continue")    
    print has_selector?("h5", :text => "How much of the premium")
    find("option", :text => "Fixed").click
    #ember class names are dynamic, so we use a regexp
    page.html =~ /Employees.+?id="(.+?)"/m && employees = $1
    fill_in(employees, :with =>  "100")
    click_link("Save and Continue")
    expect(page).to have_selector("h1", :text => "Admin Contact")
    # *** STOP EDITING HERE ***
  end
  step id: 13554,
      action: "Fill in admin contact. Complete the company info section. This is 6 steps. For EIN, use 12-345678. For SIC code, use 9999. For routing number, use 313180918. Fill out and sign the next page. ", 
      response: "Does this take you to a page where you can create email addresses for your employees?" do
    # *** START EDITING HERE ***
    #ember class names are dynamic, so we use a regexp
    page.html =~ /Name.+?id="(.+?)"/m && name = $1
    page.html =~ /Title.+?id="(.+?)"/m && title = $1
    page.html =~ /Email Address.+?id="(.+?)"/m && email_address = $1
    page.html =~ /Phone.+?id="(.+?)"/m && phone = $1
    fill_in(name, :with => "John Doe")
    fill_in(title, :with => "Manager")
    fill_in(email_address, :with => "example@example.net")
    fill_in(phone, :with => "1234567890")
    click_link("Save and Continue")
    print has_selector?("span", :text => "Step 1 of 8")
    click_link("Save and Continue")
    print has_selector?("span", :text => "Step 2 of 8")
    first("input[placeholder='Street Address']").set "123 1st Street"
    click_link("Save and Continue")
    print has_selector?("span", :text => "Step 3 of 8")
    find("option", :text => "Sole Proprietorship").click
    click_link("Save and Continue")
    print has_selector?("span", :text => "Step 4 of 8")
    print has_selector?("input")
    find("input").set "12-345678"
    click_link("Save and Continue")
    print has_selector?("span", :text => "Step 5 of 8")
    find("textarea.bizDesc").set "Lorem ipsum"
    click_link("Save and Continue")
    print has_selector?("span", :text => "Step 6 of 8")
    print has_selector?("input")
    find("input").set("9999")
    click_link("Save and Continue")
    print has_selector?("span", :text => "Step 7 of 8")
    click_link("Confirm and Continue")
    print has_selector?("span", :text => "Step 8 of 8")
    page.html =~ /Bank Name.+?id="(.+?)"/m && bank_name = $1
    page.html =~ /Routing Number.+?id="(.+?)"/m && routing_number = $1
    page.html =~ /Account Number.+?id="(.+?)"/m && account_number = $1
    fill_in(bank_name, :with => "Abc")
    fill_in(routing_number, :with => "313180918")
    fill_in(account_number, :with => "313180918")
    click_link("Agree and Continue")
    print has_selector?("span", :text => "Step 1 of 5")    
    find("input[value=no]").click
    click_link("Save and Continue")
    print has_selector?("span", :text => "Step 2 of 5")
    find("input[value=no]").click
    click_link("Save and Continue")
    print has_selector?("span", :text => "Step 3 of 5")
    find("input[value=no]").click
    find("input[name=allOwners][value=yes]").set(true)
    find("input[name=allOwners][value=no]").set(false)
    #print "*",has_selector?("input:checked[name=allOwners][value=yes]")  
    #sleep 4
    click_link("Save and Continue")
    print has_selector?("h1", :text => "then sign")
    #since the ember id's are dynamic, we use a regexp to find them
    page.html =~ /Your Name.+?id="(.+?)"/m && your_name = $1
    page.html =~ /Your Title.+?id="(.+?)"/m && your_title = $1
    fill_in(your_name, :with => "John")   
    fill_in(your_title, :with => "Manager")   
    #we turn off signature validation to get through
    execute_script('App.EnrollmentfinishController = Ember.ObjectController.extend({save: function() { var b = []; Ember.RSVP.all(b).then(function() { this.transitionToRoute("employeeemails") }.bind(this));}}); App.reset();')
    click_link("Sign Application")
    expect(page).to have_selector("h2", :text => "Employee email addresses")
    # *** STOP EDITING HERE ***
  end
  step id: 13555,
      action: "Create email addresses. Edit one of the employees email address as \"{{ random.first_name }}{{ random.number }}@e.rainforestqa.com\". Click \"Email Employees Now\" on the bottom of the next page. Continue to fill out next pages however you'd like. Click on the \"Return to Dashboard\" button.", 
      response: "Do you return to the dashboard with the Medical Insurance card in the top right NOT saying \"setup\"?" do
    # *** START EDITING HERE ***
    all("input[name=email-name]")[0].set random_email
    all("input[name=email-name]")[1].set "example2@example.net"
    all("input[type=checkbox]")[0].click
    all("input[type=checkbox]")[1].click
    click_link("Email Employees Now")
    print has_selector?("h1", :text => "Finish your enrollment")
    #I use a dummy pdf here
    File.write("/tmp/flyer.pdf", Net::HTTP.get(
      URI.parse("http://www.princexml.com/samples/flyer/flyer.pdf")))
    find("input").set "123456"
    #upload 1
    all("a", :text => "CHOOSE DOC")[0].click
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    Capybara.ignore_hidden_elements = false
    attach_file("fileUploadInput", '/tmp/flyer.pdf')
    #so we wait 10 secs (for upload) or till window closes,
    #whichever comes first
    20.times {
      #print page.driver.browser.window_handles.length
      sleep 0.5   #waiting for file upload
      break if page.driver.browser.window_handles.length==1
    }
    Capybara.ignore_hidden_elements = true
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.first)    
    #upload 2
    all("a", :text => "CHOOSE DOC")[0].click
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    Capybara.ignore_hidden_elements = false
    attach_file("fileUploadInput", '/tmp/flyer.pdf')
    #we wait 10 secs (for upload) or till window closes,
    #whichever comes first
    20.times {
      #print page.driver.browser.window_handles.length
      sleep 0.5   #waiting for file upload
      break if page.driver.browser.window_handles.length==1
    }
    Capybara.ignore_hidden_elements = true
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.first)    
    click_link("Done")
    print has_link?("Got it! Return to Dashboard")
    click_link("Got it! Return to Dashboard")
    expect(page).to have_selector("div.health a", :text => "MANAGE")
    expect(page).to have_no_selector("div.health a", :text => "SETUP")
    # *** STOP EDITING HERE ***
  end
  step id: 13556,
      action: "Wait 30 seconds, in a new tab, go to \"https://e.rainforestqa.com/{{ random.first_name }}{{ random.number }}\". Go to the link in the email. Follow the sign-up process and pick whatever email address and password you like.", 
      response: "Are you redirected to a dashboard that has a card with a Medical Insurance button with choices to either \"Decline\" or \"Enroll\"?" do
    # *** START EDITING HERE ***
    sleep 30  #because we were asked to
    visit "https://e.rainforestqa.com/#{random_email.sub("@e.rainforestqa.com","")}"
    print page.has_text?("https://secure.zenefits.com/accounts")
    page.text =~ /(https:\/\/secure.zenefits.com\/accounts.*?) /
    visit $1
    print has_selector?("h1", :text => "Let's get started")    
    fill_in("id_email", :with => "example#{rand(1000000000000000000000)}@a.com")
    fill_in("id_password1", :with => "Pa$$w0rd")
    fill_in("id_password2", :with => "Pa$$w0rd")
    check("id_tos")
    check("id_electronicDisclosure")
    click_button("Sign Up")   
    expect(page).to have_selector("div.health a", :text => "DECLINE")
    expect(page).to have_selector("div.health a", :text => "ENROLL") 
    # *** STOP EDITING HERE ***
  end
  step id: 13576,
      action: "Select \"Enroll\". Go through the set-up process. Make sure to pick at least one insurance plan for each type of insurance. Do NOT hit decline coverage.", 
      response: "Do you get to a page with a \"Begin Enrollment\" button?" do
    # *** START EDITING HERE ***
    find("div.health a", :text => "ENROLL").click
    print has_selector?("h1", :text => "confirm some info")
    find("input[placeholder='MM/DD/YYYY']").set "01/01/1980"
    click_link("Submit")
    print has_selector?("h1", :text => "Would you like to enroll in health")
    print has_selector?(".controls a.add")
    first(".controls a.add").click
    print has_selector?(".dialog .button", :text => "Enroll")
    find(".dialog .button", :text => "Enroll").click
    expect(page).to have_link("Begin Enrollment")    
    # *** STOP EDITING HERE ***
  end
  step id: 13577,
      action: "Go through the enrollment process, filling in all fields appropriately, when you get to the two questions related to previous coverage, answer no. Hit the finish enrollment button, and fill out and sign the next page. Return to the dashboard.", 
      response: "Do you see a card in the top right that has \"Medical\" in it and a \"Manage button\"?" do
    # *** START EDITING HERE ***
    click_link("Begin Enrollment")
    print has_selector?("span", :text => "Step 1 of 4")
    print has_selector?("input[placeholder='___-__-____']")
    find("input[placeholder='___-__-____']").set "123-45-6789"
    find("option", :text => "Male").click
    click_link("Save and Continue")
    print has_selector?("span", :text => "Step 2 of 4")
    find("input[placeholder='Phone']").set "(123) 456-7890"
    find("input[placeholder='Street Address']").set "123 1st Street"
    click_link("Save and Continue")
    print has_selector?("span", :text => "Step 3 of 4")
    find("option", :text => "Single").click
    find("input[value=no]").click
    click_link("Save and Continue")
    print has_selector?("span", :text => "Step 4 of 4")
    page.html =~ /Hire Date.+?id="(.+?)"/m && hire_date = $1
    fill_in(hire_date, :with =>  "01/01/2000")
    page.html =~ /Job Title.+?id="(.+?)"/m && job_title = $1
    fill_in(job_title, :with =>  "Manager")
    click_link("Save and Continue")
    print has_selector?("span", :text => "Step 1 of 3")
    click_link("Save and Continue")
    print has_selector?("span", :text => "Step 2 of 3")
    click_link("Save and Continue")
    print has_selector?("span", :text => "Step 3 of 3")
    #we say height is 7 feet and 7 inches
    all("option.ember-view", :text => "7").each { |option|
      option.click
    }
    find("input[placeholder='#']").set "50"
    click_link("Save and Continue")
    print has_link?("Sign Application")
    #turn off ember.js signature validation
    page.execute_script('App.EmployeeEnrollmentfinishController=_TransactionSavingController.extend({save:function(){var b=this.get("signature");var c="",e=this.get("model"),c=this.get("chosenMedicalPlan").get("stateCarrier").get("displayName"),e=new Date,f=e.getMonth()+1+"/"+e.getDate()+"/"+e.getFullYear(),e=this.get("currentEnrollment"),g=this.get("currentDentalEnrollment"),l=this.get("currentVisionEnrollment"),m=!1,n=!1,q=!1,t=!1,v=Ember.A();e&&e.get("status")&&"complete"!=e.get("status")&&"decline"!=e.get("status")&&this.get("chosenMedicalPlan")&&(v.pushObject(e),e.get("isSwitchCarrierEnrollment")&&(t=!0),m=!0);g&&g.get("status")&&"complete"!=g.get("status")&&"decline"!=g.get("status")&&this.get("chosenDentalPlan")&&(v.pushObject(g),g.get("isSwitchCarrierEnrollment")&&(t=!0),n=!0);l&&l.get("status")&&"complete"!=l.get("status")&&"decline"!=l.get("status")&&this.get("chosenVisionPlan")&&(v.pushObject(l),l.get("isSwitchCarrierEnrollment")&&(t=!0),q=!0);v.forEach(function(c){c.set("status","complete");c.set("name",b.get("name"));c.set("title",b.get("title"));c.set("signature",b.get("signature"));c.set("date",f)}.bind(this));this.saveAndContinue().then(function(){this.send("showModal","employee.successfinish",1)}.bind(this))}});App.reset();')
    click_link("Sign Application")
    expect(page).to have_link("Got it! Return to Dashboard")
    click_link("Got it! Return to Dashboard")
    # *** STOP EDITING HERE ***
  end
  step id: 13799,
      action: "Hit the 'manage' button. Then click 'add dependents' on the left side. Pick any option then click 'next'.", 
      response: "Do you see a screen asking to add members to the plan?" do
    # *** START EDITING HERE ***
    print has_selector?("div.health a", :text => "MANAGE")
    find("div.health a", :text => "MANAGE").click
    print has_selector?("h1", :text => "Overview")
    click_link("Add Dependents")
    print has_selector?("h1", :text => "Add Family Members to Plan")
    first(".ember-radio-button").click
    click_link("Next")
    expect(page).to have_selector("h1", :text => "Add Family Members to Plan")
    # *** STOP EDITING HERE ***
  end
  step id: 13800,
      action: "Upload {{ random.image }}, enter any date, then click 'next'.", 
      response: "Are you taken to the next page?" do
    # *** START EDITING HERE ***
    find("input[placeholder='MM/DD/YYYY']").set "01/01/2000"
    #upload
    File.write("/tmp/marr_lic.pdf", Net::HTTP.get(
      URI.parse("http://www.princexml.com/samples/flyer/flyer.pdf")))
    find("a", :text => "CHOOSE IMAGE").click
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
    Capybara.ignore_hidden_elements = false
    attach_file("fileUploadInput", '/tmp/marr_lic.pdf')
    #we wait 10 secs (for upload) or till window closes,
    #whichever comes first
    10.times {
      #print page.driver.browser.window_handles.length
      sleep 1   #waiting for file upload
      break if page.driver.browser.window_handles.length==1
    }
    Capybara.ignore_hidden_elements = true
    page.driver.browser.switch_to.window(page.driver.browser.window_handles.first)    
    #end upload
    click_link("Next")
    expect(page).to have_selector("h1", :text => "Add Family Members")
    # *** STOP EDITING HERE ***
  end
  step id: 13801,
      action: "Add all random information and then click 'Save and Continue'.", 
      response: "Do you see enrollment options?" do
    # *** START EDITING HERE ***
    fill_in("first_name", :with => "Jane")
    fill_in("last_name", :with => "Doe")
    fill_in("email", :with => "jane#{rand(1000000000000)}@example.net")
    fill_in("socialSecurity", :with => "123-45-6789")
    find("input[placeholder='MM/DD/YYYY']").set "01/01/1950"
    find("option", :text => "Female").click
    click_link("Save and Continue")
    expect(page).to have_selector("h1", :text => "Enrollment Options")
    # *** STOP EDITING HERE ***
  end
  step id: 13802,
      action: "Click the checkbox next to the name you just added. Click 'save and continue'. Add your signature and click 'sign application'.", 
      response: "Do you see a modal asking you to return to dashboard?" do
    # *** START EDITING HERE ***
    print has_selector?("input.ember-checkbox", :minimum => 2)
    all("input.ember-checkbox")[1].click 
    click_link("Save and Continue")
    print has_selector?("h1", :text => "then sign")
    #disable ember.js signature validation
    execute_script('App.EmployeeAdddependentfinishController=_TransactionSavingController.extend({save:function(){(this.saveAndContinue().then(function(){this.send("showModal","employee.addfinishsuccess",1)}.bind(this)))}});App.reset();')
    click_link("Sign Application")
    expect(page).to have_link("Got it! Return to Dashboard")
    # *** STOP EDITING HERE ***
  end
end
