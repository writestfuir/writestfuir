# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 4825, title: "Login to Mattermark (Production)") do

  visit "http://mattermark.com/"
  step id: 12731,
      action: "Log in with username \"rainforest+test@mattermark.com\" and password \"rainforest\"", 
      response: "Did you successfully log in? Are you taken to the All Companies page?" do
    # *** START EDITING HERE ***
    click_link("Log In")
    fill_in("email", :with => "rainforest+test@mattermark.com")
    fill_in("password", :with => "rainforest")
    click_button("Sign In")
    expect(page).to have_content("All Companies")
    # *** STOP EDITING HERE ***
  end
end
