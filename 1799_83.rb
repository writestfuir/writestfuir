# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 1799, title: "Check swagger docs load") do

  visit "https://app.rnfrstqa.com/docs"
  step id: 3794,
      action: "Wait for the page to load", 
      response: "Do you see \"/clients\" listed?" do
    # *** START EDITING HERE ***
    expect(page).to have_link("/clients")
    # *** STOP EDITING HERE ***
  end
  step id: 3795,
      action: "Click on /clients", 
      response: "Does it expand to reveal more items?" do
    # *** START EDITING HERE ***
    items_be4 = all("li#resource_clients ul.endpoints li.endpoint").count
    click_link("/clients")
    items_now = all("li#resource_clients ul.endpoints li.endpoint").count
    expect(items_now).to be > items_be4
    # *** STOP EDITING HERE ***
  end
end
