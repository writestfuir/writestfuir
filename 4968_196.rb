# To execute this code, you require the rainforest_ruby_runtime. https://github.com/rainforestapp/rainforest_ruby_runtime
#
# The best way to get started is to have a look at our sample tests here:
# https://github.com/rainforestapp/sample-capybara-test
#
# Please only edit code within the `step` blocks.
#
# You can use any RSpec 3 assertion and Capybara method
#

test(id: 4968, title: "Setting up payroll") do

  visit "http://zenefits.com/"
  step id: 12988,
      action: "Fill out the entire form for a new Zenefits account with dummy information (all fields need to be filled out), with email as {{ random.email }}, company zip as either 98004, 94115, or 10003, and the phone number with 10 digits; then hit submit.", 
      response: "Are you signed in?" do
    # *** START EDITING HERE ***
    click_link("Sign Up")
    print has_selector?("h1", :text => "Create your free Zenefits account")
    fill_in("id_email", :with => "example#{Time.now.to_i}@example.net")
    fill_in("id_password", :with => "Pa$$w0rd")
    fill_in("id_companyName", :with => "Company")
    fill_in("id_companyZip", :with => "10003")
    fill_in("id_companyPhone", :with => "0123456789")
    fill_in("id_regEmployeeCount", :with => "10")
    find(:css, "#id_tos").set(true)
    find("form#redirect_form input.button").click
    expect(page).to have_selector("header a", :text => "Log Out")
    # *** STOP EDITING HERE ***
  end
  step id: 12993,
      action: "Click on get quotes in the payroll box on upper left. Then enter random number of employees and hit 'get quotes'. Select either payroll services.", 
      response: "Are you asked to start or resume a new payroll application?" do
    # *** START EDITING HERE ***
    print has_selector?("div.benefit-block.payroll a",:text => "GET QUOTES")
    find(".payroll a", :text => "GET QUOTES").click    
    print has_selector?("h1", :text => "Worry-Free Payroll")
    find("input[type=text]").set "2"
    find("input[value=no]").click
    find("div.button", :text => "Get Quotes").click
    print has_selector?("div.button", :text => "Get Intuit Payroll")
    find("div.button", :text => "Get Intuit Payroll").click   
    expect(page).to have_selector("h1", :text => "New Payroll Application")
    # *** STOP EDITING HERE ***
  end
  step id: 13308,
      action: "Click 'start' or 'resume', then fill out all information for 'Company info' and 'Payroll info'. For bank routing number, use \"021000021\". Hit save and continue.", 
      response: "Are there green checks next to all items in those two sections? Are you now on Employee info section?" do
    # *** START EDITING HERE ***
    find("div.button", :text => "Start").click
    print has_selector?("h1", :text => "Basic Info")
    find("option", :text => "Sole Proprietorship").click
    #since the ember id's are dynamic, we use a regexp to find them
    page.html =~ /Legal Name.+?id="(.+?)"/m && legal_name = $1    
    page.html =~ /EIN.+?id="(.+?)"/m && ein = $1    
    fill_in(legal_name, :with => "ABC")
    fill_in(ein, :with => "12345")
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Legal Address")
    fill_in("address", :with => "123 1st Street")
    fill_in("city", :with => "New York")
    find("option[value=CA]").click
    fill_in("zip", :with => "90210")
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Locations")
    first("input[name=address]").set("123 1st Street")
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Bank Information")
    page.html =~ /Routing Number.+?id="(.+?)"/m && routing_number = $1 
    page.html =~ /Account Number.+?id="(.+?)"/m && account_number = $1 
    fill_in(routing_number, :with => "021000021")
    fill_in(account_number, :with => "021000021")
    find("option", :text => "Savings").click
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Principal Info")
    #since the ember id's are dynamic, we use a regexp to find them
    page.html =~ /First Name.+?id="(.+?)"/m && first_name = $1 
    page.html =~ /Last Name.+?id="(.+?)"/m && last_name = $1 
    page.html =~ /Email.+?id="(.+?)"/m && email = $1 
    page.html =~ /SSN.+?id="(.+?)"/m && ssn = $1 
    page.html =~ /Date of Birth.+?id="(.+?)"/m && date_of_birth = $1 
    fill_in(first_name, :with => "John")
    fill_in(last_name, :with => "Doe")
    fill_in(email, :with => "example@example.net")
    fill_in(ssn, :with => "123-45-6789")
    fill_in(date_of_birth, :with => "01/01/1970")
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Payroll Schedule")
    find("option", :text => "Bi-weekly").click
    all("option").last.click #we pick last option
    find("input[type=radio][value=No]").click
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Previous Payroll")
    find("input[value=NO]").click
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Add Employees (1/2)")
    expect(all("li.complete").count).to eq 2
    expect(page).to have_selector("h3", :text => "enter your employee info")
    # *** STOP EDITING HERE ***
  end
  step id: 13309,
      action: "Fill out all information for 'Employee info'. Make sure all fields are filled out for both employees 1/2 and employees 2/2 (name, work location, start date, etc.). Hit save and continue.", 
      response: "Do you see a page asking to email employees/" do
    # *** START EDITING HERE ***
    first("input[placeholder='First Name']").set "John"
    first("input[placeholder='Last Name']").set "Doe"
    all("input[placeholder='First Name']").last.set "John"
    all("input[placeholder='Last Name']").last.set "Doe"
    click_link("Save and Continue")
    print has_selector?("h1", :text => "Add Employees (2/2)")
    first("input[placeholder='MM/DD/YYYY']").set "01/01/2000"
    first("input[placeholder='Salary']").set "100000"
    all("input[placeholder='MM/DD/YYYY']").last.set "01/01/2000"
    all("input[placeholder='Salary']").last.set "100000"
    click_link("Save and Continue")
    expect(page).to have_link("Email Employees Now")
    # *** STOP EDITING HERE ***
  end
  step id: 13310,
      action: "Fill out all necessary information (email addresses) and then email your employees. On the next page, click on the 'finish later' button.", 
      response: "Are you back on the dashboard?" do
    # *** START EDITING HERE ***
    #this is a genuine case where the all method is useful
    all("input[name=email-name]").each { |field| field.set "abc@def.com" }
    click_link("Email Employees Now")
    print has_link?("Done")
    click_link("Finish Later")
    expect(current_url).to eq "https://secure.zenefits.com/dashboard/#/"
    # *** STOP EDITING HERE ***
  end
end
